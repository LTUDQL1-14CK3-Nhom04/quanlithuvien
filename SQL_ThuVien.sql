
/****** Object:  Database [QUANLITHUVIEN]    Script Date: 07/01/2017 12:49:48 ******/
CREATE DATABASE [QUANLITHUVIEN]

USE [QUANLITHUVIEN]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 07/01/2017 12:49:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[username] [nvarchar](10) NOT NULL,
	[pass] [nchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[username] ASC,
	[pass] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChiTietHoaDon]    Script Date: 07/01/2017 12:49:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietHoaDon](
	[maHD] [int] NOT NULL,
	[maDG] [int] NOT NULL,
	[maphi] [int] NOT NULL,
	[tenDG] [nvarchar](50) NULL,
	[PhiThuongNien] [float] NULL,
	[PhiPhat] [float] NULL,
	[ThanhTien] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[maHD] ASC,
	[maDG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DocGia]    Script Date: 07/01/2017 12:49:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocGia](
	[maDG] [int] NOT NULL,
	[tenDG] [nvarchar](50) NULL,
	[loaiDG] [int] NULL,
	[CMND] [varchar](9) NULL,
	[SDT] [nvarchar](12) NULL,
	[gioitinh] [nchar](3) NULL,
	[diachi] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[maDG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiDG]    Script Date: 07/01/2017 12:49:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiDG](
	[maloai] [int] NOT NULL,
	[tenloai] [nvarchar](20) NULL,
	[SLmuontoida] [int] NULL,
	[TLdacbiet] [bit] NULL,
	[songaymuontoida] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[maloai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Muon]    Script Date: 07/01/2017 12:49:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Muon](
	[mamuon] [int] NOT NULL,
	[madocgia] [int] NULL,
	[ngaymuon] [datetime] NULL,
	[hanmuon] [int] NULL,
	[SLsachmuon] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[mamuon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MuonTL]    Script Date: 07/01/2017 12:49:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuonTL](
	[mamuon] [int] NULL,
	[maTL] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaiLieu]    Script Date: 07/01/2017 12:49:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaiLieu](
	[maTL] [int] NOT NULL,
	[tenTL] [nvarchar](50) NOT NULL,
	[TLDB] [bit] NULL,
	[tacgia] [nvarchar](35) NULL,
	[theloai] [nvarchar](20) NULL,
	[nhaxuatban] [nvarchar](50) NULL,
	[namXB] [int] NULL,
	[gia] [float] NULL,
	[SLuong] [int] NULL,
	[Tinhtrang] [nchar](3) NULL,
PRIMARY KEY CLUSTERED 
(
	[maTL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ThuPhi]    Script Date: 07/01/2017 12:49:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThuPhi](
	[maphi] [int] NOT NULL,
	[tenphi] [nvarchar](20) NULL,
	[maloai] [int] NULL,
	[mucphi] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[maphi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tra]    Script Date: 07/01/2017 12:49:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tra](
	[matra] [int] NOT NULL,
	[mamuon] [int] NULL,
	[maDG] [int] NULL,
	[ngaytra] [datetime] NULL,
	[sosachtra] [int] NULL,
	[maphi] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[matra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TraTL]    Script Date: 07/01/2017 12:49:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TraTL](
	[matra] [int] NULL,
	[maTL] [int] NULL
) ON [PRIMARY]

GO
INSERT [dbo].[Admin] ([username], [pass]) VALUES (N'0101010', N'DucNgo    ')
GO
INSERT [dbo].[Admin] ([username], [pass]) VALUES (N'1460630', N'KimNgan   ')
GO
INSERT [dbo].[Admin] ([username], [pass]) VALUES (N'1461101', N'NgocTuyet ')
GO
INSERT [dbo].[Admin] ([username], [pass]) VALUES (N'1461405', N'ThiKhanh  ')
GO
INSERT [dbo].[Admin] ([username], [pass]) VALUES (N'1461703', N'MaiTruc   ')
GO
INSERT [dbo].[ChiTietHoaDon] ([maHD], [maDG], [maphi], [tenDG], [PhiThuongNien], [PhiPhat], [ThanhTien]) VALUES (1, 1101, 100, N'Nguyễn Hoài An', NULL, NULL, NULL)
GO
INSERT [dbo].[ChiTietHoaDon] ([maHD], [maDG], [maphi], [tenDG], [PhiThuongNien], [PhiPhat], [ThanhTien]) VALUES (1, 1102, 100, N'Nguyễn Ngọc Ánh', NULL, NULL, NULL)
GO
INSERT [dbo].[DocGia] ([maDG], [tenDG], [loaiDG], [CMND], [SDT], [gioitinh], [diachi]) VALUES (1101, N'Nguyễn Hoài An', 1001, N'264464242', N'01641234567', N'Nam', N'25/3 Lạc ng Quân, Q.10, TP HCM')
GO
INSERT [dbo].[DocGia] ([maDG], [tenDG], [loaiDG], [CMND], [SDT], [gioitinh], [diachi]) VALUES (1102, N'Nguyễn Ngọc Ánh', 1001, N'26412560', N'01688415182', N'Nữ ', N'12/21 Võ Văn Ngân Thủ Đức, TP HCM')
GO
INSERT [dbo].[DocGia] ([maDG], [tenDG], [loaiDG], [CMND], [SDT], [gioitinh], [diachi]) VALUES (1103, N'Trương Nam Sơn', 1002, N'264123264', N'01653673874', N'Nam', N'22/5 Nguyễn Xí, Q.Bình Thạnh, TPHCM')
GO
INSERT [dbo].[DocGia] ([maDG], [tenDG], [loaiDG], [CMND], [SDT], [gioitinh], [diachi]) VALUES (1104, N'Trần Trung Hiếu', 1001, N'26466342', N'09146374631', N'Nam', N'234 Trấn não, An Phú, TPHCM')
GO
INSERT [dbo].[DocGia] ([maDG], [tenDG], [loaiDG], [CMND], [SDT], [gioitinh], [diachi]) VALUES (1105, N'Trần Bạch Tuyết', 1002, N'26475244', N'01635425635', N'Nam', N'221 hùng Vương, Q.5, TPHCM')
GO
INSERT [dbo].[LoaiDG] ([maloai], [tenloai], [SLmuontoida], [TLdacbiet], [songaymuontoida]) VALUES (1001, N'Vip', 5, 1, 14)
GO
INSERT [dbo].[LoaiDG] ([maloai], [tenloai], [SLmuontoida], [TLdacbiet], [songaymuontoida]) VALUES (1002, N'Thường', 3, 0, 5)
GO
INSERT [dbo].[Muon] ([mamuon], [madocgia], [ngaymuon], [hanmuon], [SLsachmuon]) VALUES (210, 1101, CAST(N'2016-11-20 00:00:00.000' AS DateTime), 5, 4)
GO
INSERT [dbo].[Muon] ([mamuon], [madocgia], [ngaymuon], [hanmuon], [SLsachmuon]) VALUES (211, 1103, CAST(N'2016-11-21 00:00:00.000' AS DateTime), 3, 2)
GO
INSERT [dbo].[Muon] ([mamuon], [madocgia], [ngaymuon], [hanmuon], [SLsachmuon]) VALUES (212, 1105, CAST(N'2016-11-30 00:00:00.000' AS DateTime), 3, 7)
GO
INSERT [dbo].[Muon] ([mamuon], [madocgia], [ngaymuon], [hanmuon], [SLsachmuon]) VALUES (213, 1102, CAST(N'2016-12-10 00:00:00.000' AS DateTime), 5, 3)
GO
INSERT [dbo].[Muon] ([mamuon], [madocgia], [ngaymuon], [hanmuon], [SLsachmuon]) VALUES (214, 1104, CAST(N'2016-12-19 00:00:00.000' AS DateTime), 5, 1)
GO
INSERT [dbo].[MuonTL] ([mamuon], [maTL]) VALUES (210, 1)
GO
INSERT [dbo].[MuonTL] ([mamuon], [maTL]) VALUES (210, 3)
GO
INSERT [dbo].[MuonTL] ([mamuon], [maTL]) VALUES (210, 5)
GO
INSERT [dbo].[MuonTL] ([mamuon], [maTL]) VALUES (211, 2)
GO
INSERT [dbo].[MuonTL] ([mamuon], [maTL]) VALUES (211, 1)
GO
INSERT [dbo].[MuonTL] ([mamuon], [maTL]) VALUES (211, 4)
GO
INSERT [dbo].[MuonTL] ([mamuon], [maTL]) VALUES (212, 3)
GO
INSERT [dbo].[MuonTL] ([mamuon], [maTL]) VALUES (212, 5)
GO
INSERT [dbo].[MuonTL] ([mamuon], [maTL]) VALUES (213, 2)
GO
INSERT [dbo].[MuonTL] ([mamuon], [maTL]) VALUES (213, 4)
GO
INSERT [dbo].[MuonTL] ([mamuon], [maTL]) VALUES (214, 5)
GO
INSERT [dbo].[MuonTL] ([mamuon], [maTL]) VALUES (214, 3)
GO
INSERT [dbo].[MuonTL] ([mamuon], [maTL]) VALUES (214, 2)
GO
INSERT [dbo].[TaiLieu] ([maTL], [tenTL], [TLDB], [tacgia], [theloai], [nhaxuatban], [namXB], [gia], [SLuong], [Tinhtrang]) VALUES (1, N'Code Complete 2', 1, N'Steve McConnell', N'CNTT', N'Microsoft', 2004, 873181, 3, N'còn')
GO
INSERT [dbo].[TaiLieu] ([maTL], [tenTL], [TLDB], [tacgia], [theloai], [nhaxuatban], [namXB], [gia], [SLuong], [Tinhtrang]) VALUES (2, N'Học Nhanh Đồ Họa', 0, N'Không rõ', N'CNTT', N'Văn Hóa Thông Tin', 2010, 229000, 10, N'còn')
GO
INSERT [dbo].[TaiLieu] ([maTL], [tenTL], [TLDB], [tacgia], [theloai], [nhaxuatban], [namXB], [gia], [SLuong], [Tinhtrang]) VALUES (3, N'Những Cánh Thư Chưa Khép', 0, N'Jenny Han', N'Báo sinh viên', N'NXB Thanh Niên', 2016, 75000, 5, N'còn')
GO
INSERT [dbo].[TaiLieu] ([maTL], [tenTL], [TLDB], [tacgia], [theloai], [nhaxuatban], [namXB], [gia], [SLuong], [Tinhtrang]) VALUES (4, N'Nhật Bản Có Điều Kỳ Diệu', 0, N'Nhiều Tác Giả', N'Hồi kí', N'Báo Sinh Viên VN - Hoa Học Trò', 2015, 53000, 10, N'hết')
GO
INSERT [dbo].[TaiLieu] ([maTL], [tenTL], [TLDB], [tacgia], [theloai], [nhaxuatban], [namXB], [gia], [SLuong], [Tinhtrang]) VALUES (5, N'Einstein - Cuộc Đời Và Vũ Trụ', 1, N'Walter Isaacson', N'CNTT', N'NXB Th? Gi?i', 2016, 239000, 2, N'còn')
GO
INSERT [dbo].[ThuPhi] ([maphi], [tenphi], [maloai], [mucphi]) VALUES (100, N'Phí thường niên', 1001, 20000)
GO
INSERT [dbo].[ThuPhi] ([maphi], [tenphi], [maloai], [mucphi]) VALUES (101, N'Phí trễ hạn trả', 1001, 30000)
GO
INSERT [dbo].[ThuPhi] ([maphi], [tenphi], [maloai], [mucphi]) VALUES (102, N'Phí mất tài liệu', 1001, 50000)
GO
INSERT [dbo].[ThuPhi] ([maphi], [tenphi], [maloai], [mucphi]) VALUES (103, N'Phí thường niên', 1002, 30000)
GO
INSERT [dbo].[ThuPhi] ([maphi], [tenphi], [maloai], [mucphi]) VALUES (104, N'Phí trễ hạn trả', 1002, 40000)
GO
INSERT [dbo].[Tra] ([matra], [mamuon], [maDG], [ngaytra], [sosachtra], [maphi]) VALUES (310, 210, 1101, CAST(N'2016-11-25 00:00:00.000' AS DateTime), 4, 103)
GO
INSERT [dbo].[Tra] ([matra], [mamuon], [maDG], [ngaytra], [sosachtra], [maphi]) VALUES (311, 211, 1103, CAST(N'2016-12-06 00:00:00.000' AS DateTime), 2, 100)
GO
INSERT [dbo].[Tra] ([matra], [mamuon], [maDG], [ngaytra], [sosachtra], [maphi]) VALUES (312, 212, 1105, CAST(N'2016-12-02 00:00:00.000' AS DateTime), 4, 105)
GO
INSERT [dbo].[Tra] ([matra], [mamuon], [maDG], [ngaytra], [sosachtra], [maphi]) VALUES (313, 213, 1102, CAST(N'2016-12-15 00:00:00.000' AS DateTime), 3, 100)
GO
INSERT [dbo].[Tra] ([matra], [mamuon], [maDG], [ngaytra], [sosachtra], [maphi]) VALUES (314, 214, 1104, CAST(N'2016-12-25 00:00:00.000' AS DateTime), 1, 103)
GO
INSERT [dbo].[TraTL] ([matra], [maTL]) VALUES (310, 1)
GO
INSERT [dbo].[TraTL] ([matra], [maTL]) VALUES (310, 3)
GO
INSERT [dbo].[TraTL] ([matra], [maTL]) VALUES (310, 5)
GO
INSERT [dbo].[TraTL] ([matra], [maTL]) VALUES (311, 2)
GO
INSERT [dbo].[TraTL] ([matra], [maTL]) VALUES (311, 1)
GO
INSERT [dbo].[TraTL] ([matra], [maTL]) VALUES (311, 4)
GO
INSERT [dbo].[TraTL] ([matra], [maTL]) VALUES (312, 3)
GO
INSERT [dbo].[TraTL] ([matra], [maTL]) VALUES (312, 5)
GO
INSERT [dbo].[TraTL] ([matra], [maTL]) VALUES (313, 2)
GO
INSERT [dbo].[TraTL] ([matra], [maTL]) VALUES (313, 4)
GO
INSERT [dbo].[TraTL] ([matra], [maTL]) VALUES (314, 5)
GO
INSERT [dbo].[TraTL] ([matra], [maTL]) VALUES (314, 3)
GO
INSERT [dbo].[TraTL] ([matra], [maTL]) VALUES (314, 2)
GO
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HD_DG] FOREIGN KEY([maDG])
REFERENCES [dbo].[DocGia] ([maDG])
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_HD_DG]
GO
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HD_TP] FOREIGN KEY([maphi])
REFERENCES [dbo].[ThuPhi] ([maphi])
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_HD_TP]
GO
ALTER TABLE [dbo].[DocGia]  WITH CHECK ADD  CONSTRAINT [FK_DG_ML1] FOREIGN KEY([loaiDG])
REFERENCES [dbo].[LoaiDG] ([maloai])
GO
ALTER TABLE [dbo].[DocGia] CHECK CONSTRAINT [FK_DG_ML1]
GO
ALTER TABLE [dbo].[Muon]  WITH CHECK ADD  CONSTRAINT [FK_Muon_DG] FOREIGN KEY([madocgia])
REFERENCES [dbo].[DocGia] ([maDG])
GO
ALTER TABLE [dbo].[Muon] CHECK CONSTRAINT [FK_Muon_DG]
GO
ALTER TABLE [dbo].[Muon]  WITH CHECK ADD  CONSTRAINT [FK_Tra_Muon] FOREIGN KEY([mamuon])
REFERENCES [dbo].[Muon] ([mamuon])
GO
ALTER TABLE [dbo].[Muon] CHECK CONSTRAINT [FK_Tra_Muon]
GO
ALTER TABLE [dbo].[MuonTL]  WITH CHECK ADD  CONSTRAINT [FK_TL_TL] FOREIGN KEY([mamuon])
REFERENCES [dbo].[Muon] ([mamuon])
GO
ALTER TABLE [dbo].[MuonTL] CHECK CONSTRAINT [FK_TL_TL]
GO
ALTER TABLE [dbo].[MuonTL]  WITH CHECK ADD  CONSTRAINT [FK_TL_TL1] FOREIGN KEY([maTL])
REFERENCES [dbo].[TaiLieu] ([maTL])
GO
ALTER TABLE [dbo].[MuonTL] CHECK CONSTRAINT [FK_TL_TL1]
GO
ALTER TABLE [dbo].[ThuPhi]  WITH CHECK ADD  CONSTRAINT [FK_MP_ML] FOREIGN KEY([maloai])
REFERENCES [dbo].[LoaiDG] ([maloai])
GO
ALTER TABLE [dbo].[ThuPhi] CHECK CONSTRAINT [FK_MP_ML]
GO
ALTER TABLE [dbo].[Tra]  WITH CHECK ADD  CONSTRAINT [FK_Tra_DG] FOREIGN KEY([maDG])
REFERENCES [dbo].[DocGia] ([maDG])
GO
ALTER TABLE [dbo].[Tra] CHECK CONSTRAINT [FK_Tra_DG]
GO
ALTER TABLE [dbo].[TraTL]  WITH CHECK ADD  CONSTRAINT [FK_Tra_TL] FOREIGN KEY([maTL])
REFERENCES [dbo].[TaiLieu] ([maTL])
GO
ALTER TABLE [dbo].[TraTL] CHECK CONSTRAINT [FK_Tra_TL]
GO
ALTER TABLE [dbo].[TraTL]  WITH CHECK ADD  CONSTRAINT [FK_Tra_TLieu] FOREIGN KEY([matra])
REFERENCES [dbo].[Tra] ([matra])
GO
ALTER TABLE [dbo].[TraTL] CHECK CONSTRAINT [FK_Tra_TLieu]
GO

ALTER DATABASE [QUANLITHUVIEN] SET  READ_WRITE 
GO
