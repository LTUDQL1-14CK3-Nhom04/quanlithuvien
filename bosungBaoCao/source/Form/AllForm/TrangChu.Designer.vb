﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TrangChu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HệThốngToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.dangnhapMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.dangxuatMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThoátToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.quanlyMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýĐộcGiảToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýTàiLiệuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýMượnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýTrảToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.XemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DanhSáchHóaĐơnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label2.Location = New System.Drawing.Point(166, 128)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(305, 37)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "QUẢN LÍ THƯ VIỆN"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HệThốngToolStripMenuItem, Me.quanlyMenu, Me.XemToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(653, 24)
        Me.MenuStrip1.TabIndex = 3
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HệThốngToolStripMenuItem
        '
        Me.HệThốngToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.HệThốngToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.dangnhapMenu, Me.dangxuatMenu, Me.ThoátToolStripMenuItem})
        Me.HệThốngToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.HệThốngToolStripMenuItem.Name = "HệThốngToolStripMenuItem"
        Me.HệThốngToolStripMenuItem.Size = New System.Drawing.Size(69, 20)
        Me.HệThốngToolStripMenuItem.Text = "Hệ thống"
        '
        'dangnhapMenu
        '
        Me.dangnhapMenu.Name = "dangnhapMenu"
        Me.dangnhapMenu.Size = New System.Drawing.Size(132, 22)
        Me.dangnhapMenu.Text = "Đăng nhập"
        '
        'dangxuatMenu
        '
        Me.dangxuatMenu.Enabled = False
        Me.dangxuatMenu.Name = "dangxuatMenu"
        Me.dangxuatMenu.Size = New System.Drawing.Size(132, 22)
        Me.dangxuatMenu.Text = "Đăng xuất"
        '
        'ThoátToolStripMenuItem
        '
        Me.ThoátToolStripMenuItem.ForeColor = System.Drawing.Color.Red
        Me.ThoátToolStripMenuItem.Name = "ThoátToolStripMenuItem"
        Me.ThoátToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.ThoátToolStripMenuItem.Text = "Thoát"
        '
        'quanlyMenu
        '
        Me.quanlyMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.QuảnLýĐộcGiảToolStripMenuItem, Me.QuảnLýTàiLiệuToolStripMenuItem, Me.QuảnLýMượnToolStripMenuItem, Me.QuảnLýTrảToolStripMenuItem})
        Me.quanlyMenu.Enabled = False
        Me.quanlyMenu.Name = "quanlyMenu"
        Me.quanlyMenu.Size = New System.Drawing.Size(60, 20)
        Me.quanlyMenu.Text = "Quản lý"
        '
        'QuảnLýĐộcGiảToolStripMenuItem
        '
        Me.QuảnLýĐộcGiảToolStripMenuItem.Name = "QuảnLýĐộcGiảToolStripMenuItem"
        Me.QuảnLýĐộcGiảToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.QuảnLýĐộcGiảToolStripMenuItem.Text = "Quản lý độc giả"
        '
        'QuảnLýTàiLiệuToolStripMenuItem
        '
        Me.QuảnLýTàiLiệuToolStripMenuItem.Name = "QuảnLýTàiLiệuToolStripMenuItem"
        Me.QuảnLýTàiLiệuToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.QuảnLýTàiLiệuToolStripMenuItem.Text = "Quản lý tài liệu"
        '
        'QuảnLýMượnToolStripMenuItem
        '
        Me.QuảnLýMượnToolStripMenuItem.Name = "QuảnLýMượnToolStripMenuItem"
        Me.QuảnLýMượnToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.QuảnLýMượnToolStripMenuItem.Text = "Quản lý mượn"
        '
        'QuảnLýTrảToolStripMenuItem
        '
        Me.QuảnLýTrảToolStripMenuItem.Name = "QuảnLýTrảToolStripMenuItem"
        Me.QuảnLýTrảToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.QuảnLýTrảToolStripMenuItem.Text = "Quản lý trả"
        '
        'XemToolStripMenuItem
        '
        Me.XemToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DanhSáchHóaĐơnToolStripMenuItem})
        Me.XemToolStripMenuItem.Name = "XemToolStripMenuItem"
        Me.XemToolStripMenuItem.Size = New System.Drawing.Size(43, 20)
        Me.XemToolStripMenuItem.Text = "Xem"
        '
        'DanhSáchHóaĐơnToolStripMenuItem
        '
        Me.DanhSáchHóaĐơnToolStripMenuItem.Name = "DanhSáchHóaĐơnToolStripMenuItem"
        Me.DanhSáchHóaĐơnToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.DanhSáchHóaĐơnToolStripMenuItem.Text = "Danh sách hóa đơn"
        '
        'TrangChu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(653, 351)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "TrangChu"
        Me.Text = "Trang chủ"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents HệThốngToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dangnhapMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ThoátToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents quanlyMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dangxuatMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuảnLýĐộcGiảToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuảnLýTàiLiệuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuảnLýMượnToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuảnLýTrảToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents XemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DanhSáchHóaĐơnToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
