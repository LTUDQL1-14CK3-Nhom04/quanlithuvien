﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ThuPhi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtMaPhieuThu = New System.Windows.Forms.TextBox()
        Me.txtPhiThuongNien = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtPhiPhat = New System.Windows.Forms.TextBox()
        Me.btnLuu = New System.Windows.Forms.Button()
        Me.cbbMaDG = New System.Windows.Forms.ComboBox()
        Me.txtThanhTien = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtLoaiDG = New System.Windows.Forms.TextBox()
        Me.txtTenDG = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dtNgayThu = New System.Windows.Forms.DateTimePicker()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label1.Location = New System.Drawing.Point(199, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(162, 31)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "PHIẾU THU"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label2.Location = New System.Drawing.Point(76, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Mã phiếu thu"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label7.Location = New System.Drawing.Point(75, 202)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Loại độc giả"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label8.Location = New System.Drawing.Point(76, 120)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(61, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Mã độc giả"
        '
        'txtMaPhieuThu
        '
        Me.txtMaPhieuThu.Location = New System.Drawing.Point(180, 73)
        Me.txtMaPhieuThu.Name = "txtMaPhieuThu"
        Me.txtMaPhieuThu.Size = New System.Drawing.Size(182, 20)
        Me.txtMaPhieuThu.TabIndex = 8
        '
        'txtPhiThuongNien
        '
        Me.txtPhiThuongNien.Location = New System.Drawing.Point(179, 239)
        Me.txtPhiThuongNien.Name = "txtPhiThuongNien"
        Me.txtPhiThuongNien.Size = New System.Drawing.Size(182, 20)
        Me.txtPhiThuongNien.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label4.Location = New System.Drawing.Point(75, 242)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(83, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Phí thường niên"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label9.Location = New System.Drawing.Point(75, 285)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(48, 13)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Phí phạt"
        '
        'txtPhiPhat
        '
        Me.txtPhiPhat.Location = New System.Drawing.Point(179, 282)
        Me.txtPhiPhat.Name = "txtPhiPhat"
        Me.txtPhiPhat.Size = New System.Drawing.Size(182, 20)
        Me.txtPhiPhat.TabIndex = 17
        '
        'btnLuu
        '
        Me.btnLuu.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnLuu.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnLuu.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnLuu.Location = New System.Drawing.Point(394, 180)
        Me.btnLuu.Name = "btnLuu"
        Me.btnLuu.Size = New System.Drawing.Size(90, 47)
        Me.btnLuu.TabIndex = 18
        Me.btnLuu.Text = "Tính tiền"
        Me.btnLuu.UseVisualStyleBackColor = False
        '
        'cbbMaDG
        '
        Me.cbbMaDG.FormattingEnabled = True
        Me.cbbMaDG.Location = New System.Drawing.Point(180, 111)
        Me.cbbMaDG.Name = "cbbMaDG"
        Me.cbbMaDG.Size = New System.Drawing.Size(182, 21)
        Me.cbbMaDG.TabIndex = 20
        '
        'txtThanhTien
        '
        Me.txtThanhTien.Enabled = False
        Me.txtThanhTien.Location = New System.Drawing.Point(179, 325)
        Me.txtThanhTien.Name = "txtThanhTien"
        Me.txtThanhTien.Size = New System.Drawing.Size(182, 20)
        Me.txtThanhTien.TabIndex = 23
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label3.Location = New System.Drawing.Point(75, 328)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 13)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "Tổng tiền"
        '
        'txtLoaiDG
        '
        Me.txtLoaiDG.Enabled = False
        Me.txtLoaiDG.Location = New System.Drawing.Point(179, 195)
        Me.txtLoaiDG.Name = "txtLoaiDG"
        Me.txtLoaiDG.Size = New System.Drawing.Size(182, 20)
        Me.txtLoaiDG.TabIndex = 24
        '
        'txtTenDG
        '
        Me.txtTenDG.Enabled = False
        Me.txtTenDG.Location = New System.Drawing.Point(179, 154)
        Me.txtTenDG.Name = "txtTenDG"
        Me.txtTenDG.Size = New System.Drawing.Size(182, 20)
        Me.txtTenDG.TabIndex = 26
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label5.Location = New System.Drawing.Point(75, 161)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(65, 13)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Tên độc giả"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label6.Location = New System.Drawing.Point(76, 368)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 13)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Ngày thu"
        '
        'dtNgayThu
        '
        Me.dtNgayThu.CustomFormat = "dd/MM/yyyy"
        Me.dtNgayThu.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtNgayThu.Location = New System.Drawing.Point(179, 362)
        Me.dtNgayThu.Name = "dtNgayThu"
        Me.dtNgayThu.Size = New System.Drawing.Size(182, 20)
        Me.dtNgayThu.TabIndex = 28
        '
        'ThuPhi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(507, 460)
        Me.Controls.Add(Me.dtNgayThu)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtTenDG)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtLoaiDG)
        Me.Controls.Add(Me.txtThanhTien)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cbbMaDG)
        Me.Controls.Add(Me.btnLuu)
        Me.Controls.Add(Me.txtPhiPhat)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtPhiThuongNien)
        Me.Controls.Add(Me.txtMaPhieuThu)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "ThuPhi"
        Me.Text = "Thu phí độc giả"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtMaPhieuThu As System.Windows.Forms.TextBox
    Friend WithEvents txtPhiThuongNien As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtPhiPhat As System.Windows.Forms.TextBox
    Friend WithEvents btnLuu As System.Windows.Forms.Button
    Friend WithEvents cbbMaDG As System.Windows.Forms.ComboBox
    Friend WithEvents txtThanhTien As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtLoaiDG As System.Windows.Forms.TextBox
    Friend WithEvents txtTenDG As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dtNgayThu As System.Windows.Forms.DateTimePicker
End Class
