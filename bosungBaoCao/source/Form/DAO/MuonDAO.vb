﻿Imports DTO
Public Class MuonDAO
    Private dbs As New DALDataContext()
    Public Function getAll() As List(Of Muon)
        Return dbs.Muons.ToList()
    End Function
    Public Function them(item As Muon) As Boolean
        dbs = New DALDataContext()
        dbs.Muons.InsertOnSubmit(item)
        dbs.SubmitChanges()
        Return True

    End Function
    Public Function sua(item As Muon) As Boolean
        dbs = New DALDataContext()
        Dim old = (From c In dbs.Muons
                Where c.mamuon = item.mamuon
                Select c).First
        old.madocgia = item.madocgia
        old.maTL = item.maTL
        old.hanmuon = item.hanmuon
        old.ngaymuon = item.ngaymuon

        dbs.SubmitChanges()
        Return True
    End Function
    Public Function xoa(item As Muon) As Boolean
        dbs = New DALDataContext()
        Dim old = (From c In dbs.Muons
                Where c.mamuon = item.mamuon
                Select c).First
        dbs.Muons.DeleteOnSubmit(old)
        dbs.SubmitChanges()
        Return True

    End Function
End Class
