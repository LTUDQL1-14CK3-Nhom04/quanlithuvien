﻿Imports DTO
Public Class TraDAO
    Private dbs As New DALDataContext()
    Public Function getAll() As List(Of Tra)
        Return dbs.Tras.ToList()
    End Function
    Public Function them(item As Tra) As Boolean
        dbs = New DALDataContext()
        dbs.Tras.InsertOnSubmit(item)
        dbs.SubmitChanges()
        Return True

    End Function
    Public Function sua(item As Tra) As Boolean
        dbs = New DALDataContext()
        Dim old = (From c In dbs.Tras
                Where c.matra = item.matra
                Select c).First
        old.mamuon = item.mamuon
        old.ngaytra = item.ngaytra

        dbs.SubmitChanges()
        Return True
    End Function
    Public Function xoa(item As Tra) As Boolean
        dbs = New DALDataContext()
        Dim old = (From c In dbs.Tras
                Where c.matra = item.matra
                Select c).First
        dbs.Tras.DeleteOnSubmit(old)
        dbs.SubmitChanges()
        Return True

    End Function
End Class
