﻿Imports DTO
Public Class TaiLieuDAO
    Private dbs As New DALDataContext()
    Public Function getAll() As List(Of TaiLieu)
        Return dbs.TaiLieus.ToList()
    End Function
    Public Function tim(text As String) As List(Of TaiLieu)
        text = text.ToLower()
        dbs = New DALDataContext()
        Dim lst = From c In dbs.TaiLieus
                  Where c.tenTL.ToLower().Contains(text) Or c.theloai.ToLower().Contains(text) Or c.tacgia.ToLower().Contains(text) Or c.nhaxuatban.ToLower().Contains(text)
                  Select c
        Return lst.ToList()
    End Function
    Public Function them(tailieu As TaiLieu) As Boolean
        dbs = New DALDataContext()
        dbs.TaiLieus.InsertOnSubmit(tailieu)
        dbs.SubmitChanges()
        Return True

    End Function
    Public Function sua(tailieu As TaiLieu) As Boolean
        dbs = New DALDataContext()
        Dim old = (From c In dbs.TaiLieus
                Where c.maTL = tailieu.maTL
                Select c).First
        old.tenTL = tailieu.tenTL
        old.namXB = tailieu.namXB
        old.gia = tailieu.gia
        old.nhaxuatban = tailieu.nhaxuatban
        old.SLuong = tailieu.SLuong
        old.tacgia = tailieu.tacgia
        old.theloai = tailieu.theloai
        old.Tinhtrang = tailieu.Tinhtrang
        old.TLDB = tailieu.TLDB
        dbs.SubmitChanges()
        Return True
    End Function
    Public Function xoa(tailieu As TaiLieu) As Boolean
        dbs = New DALDataContext()
        Dim old = (From c In dbs.TaiLieus
                Where c.maTL = tailieu.maTL
                Select c).First
        dbs.TaiLieus.DeleteOnSubmit(old)
        dbs.SubmitChanges()
        Return True

    End Function
End Class
