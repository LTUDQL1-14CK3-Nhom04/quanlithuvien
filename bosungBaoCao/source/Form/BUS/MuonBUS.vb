﻿Imports DAO
Imports DTO
Public Class MuonBUS
    Private dao As New MuonDAO()
    Public Function getAll() As List(Of Muon)
        Return dao.getAll()
    End Function
    Public Function them(item As Muon) As Boolean
        Return dao.them(item)
    End Function
    Public Function sua(item As Muon) As Boolean
        Return dao.sua(item)
    End Function
    Public Function xoa(item As Muon) As Boolean
        Return dao.xoa(item)
    End Function
End Class
