﻿Imports DAO
Imports DTO
Public Class TraBUS
    Private dao As New TraDAO()
    Public Function getAll() As List(Of Tra)
        Return dao.getAll()
    End Function
    Public Function them(item As Tra) As Boolean
        Return dao.them(item)
    End Function
    Public Function sua(item As Tra) As Boolean
        Return dao.sua(item)
    End Function
    Public Function xoa(item As Tra) As Boolean
        Return dao.xoa(item)
    End Function
End Class
