﻿Imports DAO
Imports DTO
Public Class ChiTietHoaDonBUS
    Private dao As New HoaDonDAO()
    Public Function getAll() As List(Of ChiTietHoaDon)
        Return dao.getAll()
    End Function
    Public Function them(item As ChiTietHoaDon) As Boolean
        Return dao.them(item)
    End Function
    Public Function sua(item As ChiTietHoaDon) As Boolean
        Return dao.sua(item)
    End Function
    Public Function xoa(item As ChiTietHoaDon) As Boolean
        Return dao.xoa(item)
    End Function
End Class
