﻿Imports DAO
Imports DTO
Public Class DocGiaBUS

    Dim cnDAO As New DocGiaDAO()
    Public Function XemDanhSachDocGia() As DataTable
        Return cnDAO.XemDanhSachDocGia()

    End Function
    Public Function GetAll() As List(Of DocGia)
        Return cnDAO.GetAll()
    End Function
    Public Function TimDocGiaTheoMa(maDG As Integer) As DataTable
        Return cnDAO.TimDocGiaTheoMa(maDG)
    End Function
    Public Function TimDocGiaTheoTen(tenDG As String) As DataTable
        Return cnDAO.TimDocGiaTheoTen(tenDG)
    End Function
    Public Function TimDocGiaTheoCMND(cmnd As String) As DataTable
        Return cnDAO.TimDocGiaTheoCMND(cmnd)
    End Function
    Public Function loadCBLoaiDG() As DataTable
        Return cnDAO.loadCBLoaiDG()

    End Function
    Public Function loadDgvDocGia() As DataTable
        Return cnDAO.loaddgv()
    End Function
    Public Function themDG(maDG As Integer, tenDG As String, loaiDG As Integer, CMND As String, SDT As String, diachi As String) As Boolean
        Return cnDAO.themDG(maDG, tenDG, loaiDG, CMND, SDT, diachi)
    End Function
    Public Function suaDG(maDG As Integer, tenDG As String, loaiDG As Integer, CMND As String, SDT As String, diachi As String) As Boolean
        Return cnDAO.suaDG(maDG, tenDG, loaiDG, CMND, SDT, diachi)
    End Function
    Public Function xoaDG(maDG As Integer) As Boolean
        Return cnDAO.xoaDG(maDG)
    End Function
End Class
