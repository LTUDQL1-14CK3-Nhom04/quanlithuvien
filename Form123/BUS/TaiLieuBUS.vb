﻿Imports DAO
Imports DTO

Public Class TaiLieuBUS
    Private tailieuDao As New TaiLieuDAO()
    Public Function getAll() As List(Of TaiLieu)
        Return tailieuDao.getAll()
    End Function
    Public Function tim(text As String) As List(Of TaiLieu)
        Return tailieuDao.tim(text)
    End Function
    Public Function them(tailieu As TaiLieu) As Boolean
        Return tailieuDao.them(tailieu)
    End Function
    Public Function sua(tailieu As TaiLieu) As Boolean
        Return tailieuDao.sua(tailieu)
    End Function
    Public Function xoa(tailieu As TaiLieu) As Boolean
        Return tailieuDao.xoa(tailieu)
    End Function
End Class
