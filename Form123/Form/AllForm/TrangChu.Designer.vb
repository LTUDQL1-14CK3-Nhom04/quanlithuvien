﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TrangChu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbTimKiem = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnTimKiem = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtTimKiem = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnQLDG = New System.Windows.Forms.Button()
        Me.btnThoat = New System.Windows.Forms.Button()
        Me.btnQLMT = New System.Windows.Forms.Button()
        Me.btnQLTL = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cbTimKiem
        '
        Me.cbTimKiem.FormattingEnabled = True
        Me.cbTimKiem.Location = New System.Drawing.Point(156, 34)
        Me.cbTimKiem.Name = "cbTimKiem"
        Me.cbTimKiem.Size = New System.Drawing.Size(155, 21)
        Me.cbTimKiem.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(28, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Tìm theo"
        '
        'btnTimKiem
        '
        Me.btnTimKiem.BackColor = System.Drawing.SystemColors.ControlText
        Me.btnTimKiem.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnTimKiem.Location = New System.Drawing.Point(187, 123)
        Me.btnTimKiem.Name = "btnTimKiem"
        Me.btnTimKiem.Size = New System.Drawing.Size(99, 23)
        Me.btnTimKiem.TabIndex = 2
        Me.btnTimKiem.Text = "Tìm"
        Me.btnTimKiem.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtTimKiem)
        Me.GroupBox1.Controls.Add(Me.btnTimKiem)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cbTimKiem)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(74, 85)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(331, 164)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Thông tin tìm kiếm"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(28, 82)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(108, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nội dung tìm kiếm"
        '
        'txtTimKiem
        '
        Me.txtTimKiem.Location = New System.Drawing.Point(156, 79)
        Me.txtTimKiem.Name = "txtTimKiem"
        Me.txtTimKiem.Size = New System.Drawing.Size(155, 20)
        Me.txtTimKiem.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label2.Location = New System.Drawing.Point(147, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(274, 31)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "QUẢN LÍ THƯ VIỆN"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox2.Controls.Add(Me.btnQLDG)
        Me.GroupBox2.Controls.Add(Me.btnThoat)
        Me.GroupBox2.Controls.Add(Me.btnQLMT)
        Me.GroupBox2.Controls.Add(Me.btnQLTL)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(441, 52)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(200, 233)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Danh sách chức năng"
        Me.GroupBox2.UseWaitCursor = True
        '
        'btnQLDG
        '
        Me.btnQLDG.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnQLDG.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnQLDG.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnQLDG.Location = New System.Drawing.Point(41, 75)
        Me.btnQLDG.Name = "btnQLDG"
        Me.btnQLDG.Size = New System.Drawing.Size(130, 27)
        Me.btnQLDG.TabIndex = 8
        Me.btnQLDG.Text = "Quản lí độc giả"
        Me.btnQLDG.UseVisualStyleBackColor = False
        Me.btnQLDG.UseWaitCursor = True
        '
        'btnThoat
        '
        Me.btnThoat.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnThoat.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnThoat.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnThoat.Location = New System.Drawing.Point(41, 172)
        Me.btnThoat.Name = "btnThoat"
        Me.btnThoat.Size = New System.Drawing.Size(130, 27)
        Me.btnThoat.TabIndex = 7
        Me.btnThoat.Text = "Thoát"
        Me.btnThoat.UseVisualStyleBackColor = False
        Me.btnThoat.UseWaitCursor = True
        '
        'btnQLMT
        '
        Me.btnQLMT.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnQLMT.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnQLMT.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnQLMT.Location = New System.Drawing.Point(41, 126)
        Me.btnQLMT.Name = "btnQLMT"
        Me.btnQLMT.Size = New System.Drawing.Size(130, 27)
        Me.btnQLMT.TabIndex = 6
        Me.btnQLMT.Text = "Quản lí mượn trả"
        Me.btnQLMT.UseVisualStyleBackColor = False
        Me.btnQLMT.UseWaitCursor = True
        '
        'btnQLTL
        '
        Me.btnQLTL.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnQLTL.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnQLTL.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnQLTL.Location = New System.Drawing.Point(41, 28)
        Me.btnQLTL.Name = "btnQLTL"
        Me.btnQLTL.Size = New System.Drawing.Size(130, 27)
        Me.btnQLTL.TabIndex = 5
        Me.btnQLTL.Text = "Quản lí tài liệu"
        Me.btnQLTL.UseVisualStyleBackColor = False
        Me.btnQLTL.UseWaitCursor = True
        '
        'TrangChu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(653, 351)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "TrangChu"
        Me.Text = "Trang chủ"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbTimKiem As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnTimKiem As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTimKiem As System.Windows.Forms.TextBox
    Friend WithEvents btnQLDG As System.Windows.Forms.Button
    Friend WithEvents btnThoat As System.Windows.Forms.Button
    Friend WithEvents btnQLMT As System.Windows.Forms.Button
    Friend WithEvents btnQLTL As System.Windows.Forms.Button
End Class
