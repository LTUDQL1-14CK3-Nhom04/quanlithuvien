﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ThuPhi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtKetQuaTinh = New System.Windows.Forms.TextBox()
        Me.btnTinhTien = New System.Windows.Forms.Button()
        Me.btnXuatHoaDon = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtSSQuaHan = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtPhiPhat = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtLoaiDocGia = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtMaDocGia = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtMaPhieuThu = New System.Windows.Forms.TextBox()
        Me.txtDongPhi = New System.Windows.Forms.TextBox()
        Me.txtSSDangMuon = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label1.Location = New System.Drawing.Point(294, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(171, 31)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "PHIẾU THU"
        '
        'txtKetQuaTinh
        '
        Me.txtKetQuaTinh.Location = New System.Drawing.Point(35, 45)
        Me.txtKetQuaTinh.Name = "txtKetQuaTinh"
        Me.txtKetQuaTinh.Size = New System.Drawing.Size(145, 20)
        Me.txtKetQuaTinh.TabIndex = 13
        '
        'btnTinhTien
        '
        Me.btnTinhTien.BackColor = System.Drawing.SystemColors.ControlText
        Me.btnTinhTien.ForeColor = System.Drawing.SystemColors.InactiveBorder
        Me.btnTinhTien.Location = New System.Drawing.Point(65, 86)
        Me.btnTinhTien.Name = "btnTinhTien"
        Me.btnTinhTien.Size = New System.Drawing.Size(103, 26)
        Me.btnTinhTien.TabIndex = 18
        Me.btnTinhTien.Text = "Tính tiền"
        Me.btnTinhTien.UseVisualStyleBackColor = False
        '
        'btnXuatHoaDon
        '
        Me.btnXuatHoaDon.BackColor = System.Drawing.SystemColors.ControlText
        Me.btnXuatHoaDon.ForeColor = System.Drawing.SystemColors.InactiveBorder
        Me.btnXuatHoaDon.Location = New System.Drawing.Point(65, 129)
        Me.btnXuatHoaDon.Name = "btnXuatHoaDon"
        Me.btnXuatHoaDon.Size = New System.Drawing.Size(103, 26)
        Me.btnXuatHoaDon.TabIndex = 19
        Me.btnXuatHoaDon.Text = "Xuất hóa đơn"
        Me.btnXuatHoaDon.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox1.Controls.Add(Me.txtSSQuaHan)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtPhiPhat)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtLoaiDocGia)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.txtMaDocGia)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.txtMaPhieuThu)
        Me.GroupBox1.Controls.Add(Me.txtDongPhi)
        Me.GroupBox1.Controls.Add(Me.txtSSDangMuon)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GroupBox1.Location = New System.Drawing.Point(139, 81)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(349, 318)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Thông tin phiếu thu"
        '
        'txtSSQuaHan
        '
        Me.txtSSQuaHan.Location = New System.Drawing.Point(144, 190)
        Me.txtSSQuaHan.Name = "txtSSQuaHan"
        Me.txtSSQuaHan.Size = New System.Drawing.Size(182, 20)
        Me.txtSSQuaHan.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label3.Location = New System.Drawing.Point(23, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Mã phiếu thu"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label10.Location = New System.Drawing.Point(23, 236)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(60, 13)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Đóng phí"
        '
        'txtPhiPhat
        '
        Me.txtPhiPhat.Location = New System.Drawing.Point(144, 279)
        Me.txtPhiPhat.Name = "txtPhiPhat"
        Me.txtPhiPhat.Size = New System.Drawing.Size(182, 20)
        Me.txtPhiPhat.TabIndex = 17
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label11.Location = New System.Drawing.Point(23, 197)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(103, 13)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "Số sách quá hạn"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label15.Location = New System.Drawing.Point(23, 279)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(56, 13)
        Me.Label15.TabIndex = 16
        Me.Label15.Text = "Phí phạt"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label12.Location = New System.Drawing.Point(23, 155)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(120, 13)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Số sách đang mượn"
        '
        'txtLoaiDocGia
        '
        Me.txtLoaiDocGia.Location = New System.Drawing.Point(144, 107)
        Me.txtLoaiDocGia.Name = "txtLoaiDocGia"
        Me.txtLoaiDocGia.Size = New System.Drawing.Size(182, 20)
        Me.txtLoaiDocGia.TabIndex = 15
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label13.Location = New System.Drawing.Point(23, 114)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(78, 13)
        Me.Label13.TabIndex = 6
        Me.Label13.Text = "Loại độc giả"
        '
        'txtMaDocGia
        '
        Me.txtMaDocGia.Location = New System.Drawing.Point(144, 63)
        Me.txtMaDocGia.Name = "txtMaDocGia"
        Me.txtMaDocGia.Size = New System.Drawing.Size(182, 20)
        Me.txtMaDocGia.TabIndex = 14
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label14.Location = New System.Drawing.Point(23, 70)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(71, 13)
        Me.Label14.TabIndex = 7
        Me.Label14.Text = "Mã độc giả"
        '
        'txtMaPhieuThu
        '
        Me.txtMaPhieuThu.Location = New System.Drawing.Point(144, 23)
        Me.txtMaPhieuThu.Name = "txtMaPhieuThu"
        Me.txtMaPhieuThu.Size = New System.Drawing.Size(182, 20)
        Me.txtMaPhieuThu.TabIndex = 8
        Me.txtMaPhieuThu.UseWaitCursor = True
        '
        'txtDongPhi
        '
        Me.txtDongPhi.Location = New System.Drawing.Point(144, 233)
        Me.txtDongPhi.Name = "txtDongPhi"
        Me.txtDongPhi.Size = New System.Drawing.Size(182, 20)
        Me.txtDongPhi.TabIndex = 12
        '
        'txtSSDangMuon
        '
        Me.txtSSDangMuon.Location = New System.Drawing.Point(144, 148)
        Me.txtSSDangMuon.Name = "txtSSDangMuon"
        Me.txtSSDangMuon.Size = New System.Drawing.Size(182, 20)
        Me.txtSSDangMuon.TabIndex = 9
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox2.Controls.Add(Me.txtKetQuaTinh)
        Me.GroupBox2.Controls.Add(Me.btnTinhTien)
        Me.GroupBox2.Controls.Add(Me.btnXuatHoaDon)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(520, 123)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(208, 191)
        Me.GroupBox2.TabIndex = 21
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Kết quả"
        '
        'ThuPhi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(769, 421)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "ThuPhi"
        Me.Text = "Thu phí độc giả"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtKetQuaTinh As System.Windows.Forms.TextBox
    Friend WithEvents btnTinhTien As System.Windows.Forms.Button
    Friend WithEvents btnXuatHoaDon As Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSSQuaHan As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtPhiPhat As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtLoaiDocGia As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtMaDocGia As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtMaPhieuThu As System.Windows.Forms.TextBox
    Friend WithEvents txtDongPhi As System.Windows.Forms.TextBox
    Friend WithEvents txtSSDangMuon As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
End Class
