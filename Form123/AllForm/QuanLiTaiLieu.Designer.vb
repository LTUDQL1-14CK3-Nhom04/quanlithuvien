﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class QuanLiTaiLieu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dgvTaiLieu = New System.Windows.Forms.DataGridView()
        Me.maTL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tenTL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tacgia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.theloai = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nhaxuatban = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.namXB = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SLuong = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tinhtrang = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TLDB = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.groupInfor = New System.Windows.Forms.GroupBox()
        Me.chkTLDB = New System.Windows.Forms.CheckBox()
        Me.txtTheLoai = New System.Windows.Forms.TextBox()
        Me.txtNXB = New System.Windows.Forms.TextBox()
        Me.txtTenTL = New System.Windows.Forms.TextBox()
        Me.txtTacGia = New System.Windows.Forms.TextBox()
        Me.txtTinhTrang = New System.Windows.Forms.TextBox()
        Me.txtSL = New System.Windows.Forms.TextBox()
        Me.txtGia = New System.Windows.Forms.TextBox()
        Me.txtNamXB = New System.Windows.Forms.TextBox()
        Me.txtMaTL = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnThem = New System.Windows.Forms.Button()
        Me.btnXoa = New System.Windows.Forms.Button()
        Me.btnCapNhat = New System.Windows.Forms.Button()
        Me.btnThoat = New System.Windows.Forms.Button()
        Me.btnLuu = New System.Windows.Forms.Button()
        Me.btnHuy = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtTimKiem = New System.Windows.Forms.TextBox()
        Me.btnTim = New System.Windows.Forms.Button()
        CType(Me.dgvTaiLieu, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.groupInfor.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label11.Location = New System.Drawing.Point(329, 9)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(241, 31)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "QUẢN LÍ TÀI LIỆU"
        '
        'dgvTaiLieu
        '
        Me.dgvTaiLieu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTaiLieu.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.maTL, Me.tenTL, Me.tacgia, Me.theloai, Me.nhaxuatban, Me.namXB, Me.gia, Me.SLuong, Me.Tinhtrang, Me.TLDB})
        Me.dgvTaiLieu.Location = New System.Drawing.Point(12, 430)
        Me.dgvTaiLieu.Name = "dgvTaiLieu"
        Me.dgvTaiLieu.Size = New System.Drawing.Size(1028, 159)
        Me.dgvTaiLieu.TabIndex = 8
        '
        'maTL
        '
        Me.maTL.DataPropertyName = "maTL"
        Me.maTL.HeaderText = "Mã tài liệu"
        Me.maTL.Name = "maTL"
        '
        'tenTL
        '
        Me.tenTL.DataPropertyName = "tenTL"
        Me.tenTL.HeaderText = "Tên tài liệu"
        Me.tenTL.Name = "tenTL"
        '
        'tacgia
        '
        Me.tacgia.DataPropertyName = "tacgia"
        Me.tacgia.HeaderText = "Tác giả"
        Me.tacgia.Name = "tacgia"
        '
        'theloai
        '
        Me.theloai.DataPropertyName = "theloai"
        Me.theloai.HeaderText = "Thể loại"
        Me.theloai.Name = "theloai"
        '
        'nhaxuatban
        '
        Me.nhaxuatban.DataPropertyName = "nhaxuatban"
        Me.nhaxuatban.HeaderText = "Nhà xuất bản"
        Me.nhaxuatban.Name = "nhaxuatban"
        '
        'namXB
        '
        Me.namXB.DataPropertyName = "namXB"
        Me.namXB.HeaderText = "Năm xuất bản"
        Me.namXB.Name = "namXB"
        '
        'gia
        '
        Me.gia.DataPropertyName = "gia"
        Me.gia.HeaderText = "Giá bìa"
        Me.gia.Name = "gia"
        '
        'SLuong
        '
        Me.SLuong.DataPropertyName = "SLuong"
        Me.SLuong.HeaderText = "Số lượng"
        Me.SLuong.Name = "SLuong"
        '
        'Tinhtrang
        '
        Me.Tinhtrang.DataPropertyName = "Tinhtrang"
        Me.Tinhtrang.HeaderText = "Tình trạng"
        Me.Tinhtrang.Name = "Tinhtrang"
        '
        'TLDB
        '
        Me.TLDB.DataPropertyName = "TLDB"
        Me.TLDB.HeaderText = "Tài liệu đặc biệt"
        Me.TLDB.Name = "TLDB"
        '
        'groupInfor
        '
        Me.groupInfor.Controls.Add(Me.chkTLDB)
        Me.groupInfor.Controls.Add(Me.txtTheLoai)
        Me.groupInfor.Controls.Add(Me.txtNXB)
        Me.groupInfor.Controls.Add(Me.txtTenTL)
        Me.groupInfor.Controls.Add(Me.txtTacGia)
        Me.groupInfor.Controls.Add(Me.txtTinhTrang)
        Me.groupInfor.Controls.Add(Me.txtSL)
        Me.groupInfor.Controls.Add(Me.txtGia)
        Me.groupInfor.Controls.Add(Me.txtNamXB)
        Me.groupInfor.Controls.Add(Me.txtMaTL)
        Me.groupInfor.Controls.Add(Me.Label9)
        Me.groupInfor.Controls.Add(Me.Label8)
        Me.groupInfor.Controls.Add(Me.Label7)
        Me.groupInfor.Controls.Add(Me.Label6)
        Me.groupInfor.Controls.Add(Me.Label5)
        Me.groupInfor.Controls.Add(Me.Label4)
        Me.groupInfor.Controls.Add(Me.Label3)
        Me.groupInfor.Controls.Add(Me.Label2)
        Me.groupInfor.Controls.Add(Me.Label1)
        Me.groupInfor.Location = New System.Drawing.Point(175, 49)
        Me.groupInfor.Name = "groupInfor"
        Me.groupInfor.Size = New System.Drawing.Size(518, 311)
        Me.groupInfor.TabIndex = 9
        Me.groupInfor.TabStop = False
        Me.groupInfor.Text = "Thông tin tài liệu"
        '
        'chkTLDB
        '
        Me.chkTLDB.AutoSize = True
        Me.chkTLDB.Location = New System.Drawing.Point(125, 288)
        Me.chkTLDB.Name = "chkTLDB"
        Me.chkTLDB.Size = New System.Drawing.Size(102, 17)
        Me.chkTLDB.TabIndex = 20
        Me.chkTLDB.Text = "Tài liệu đặc biệt"
        Me.chkTLDB.UseVisualStyleBackColor = True
        '
        'txtTheLoai
        '
        Me.txtTheLoai.Location = New System.Drawing.Point(106, 121)
        Me.txtTheLoai.Name = "txtTheLoai"
        Me.txtTheLoai.Size = New System.Drawing.Size(214, 20)
        Me.txtTheLoai.TabIndex = 19
        '
        'txtNXB
        '
        Me.txtNXB.Location = New System.Drawing.Point(106, 147)
        Me.txtNXB.Name = "txtNXB"
        Me.txtNXB.Size = New System.Drawing.Size(214, 20)
        Me.txtNXB.TabIndex = 18
        '
        'txtTenTL
        '
        Me.txtTenTL.Location = New System.Drawing.Point(106, 59)
        Me.txtTenTL.Name = "txtTenTL"
        Me.txtTenTL.Size = New System.Drawing.Size(214, 20)
        Me.txtTenTL.TabIndex = 17
        '
        'txtTacGia
        '
        Me.txtTacGia.Location = New System.Drawing.Point(106, 89)
        Me.txtTacGia.Name = "txtTacGia"
        Me.txtTacGia.Size = New System.Drawing.Size(214, 20)
        Me.txtTacGia.TabIndex = 16
        '
        'txtTinhTrang
        '
        Me.txtTinhTrang.Location = New System.Drawing.Point(106, 259)
        Me.txtTinhTrang.Name = "txtTinhTrang"
        Me.txtTinhTrang.Size = New System.Drawing.Size(214, 20)
        Me.txtTinhTrang.TabIndex = 14
        '
        'txtSL
        '
        Me.txtSL.Location = New System.Drawing.Point(106, 228)
        Me.txtSL.Name = "txtSL"
        Me.txtSL.Size = New System.Drawing.Size(214, 20)
        Me.txtSL.TabIndex = 13
        '
        'txtGia
        '
        Me.txtGia.Location = New System.Drawing.Point(106, 200)
        Me.txtGia.Name = "txtGia"
        Me.txtGia.Size = New System.Drawing.Size(214, 20)
        Me.txtGia.TabIndex = 12
        '
        'txtNamXB
        '
        Me.txtNamXB.Location = New System.Drawing.Point(106, 174)
        Me.txtNamXB.Name = "txtNamXB"
        Me.txtNamXB.Size = New System.Drawing.Size(214, 20)
        Me.txtNamXB.TabIndex = 11
        '
        'txtMaTL
        '
        Me.txtMaTL.Location = New System.Drawing.Point(106, 30)
        Me.txtMaTL.Name = "txtMaTL"
        Me.txtMaTL.Size = New System.Drawing.Size(214, 20)
        Me.txtMaTL.TabIndex = 10
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(21, 66)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(59, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Tên tài liệu"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(21, 154)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Nhà xuất bản"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(21, 184)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(73, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Năm xuất bản"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(21, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Tác giả"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(21, 128)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Thể loại"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(21, 210)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Giá bìa"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(21, 235)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Số lượng"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 266)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tình trạng"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(21, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Mã tài liệu"
        '
        'btnThem
        '
        Me.btnThem.Location = New System.Drawing.Point(727, 76)
        Me.btnThem.Name = "btnThem"
        Me.btnThem.Size = New System.Drawing.Size(75, 23)
        Me.btnThem.TabIndex = 10
        Me.btnThem.Text = "Thêm"
        Me.btnThem.UseVisualStyleBackColor = True
        '
        'btnXoa
        '
        Me.btnXoa.Location = New System.Drawing.Point(727, 145)
        Me.btnXoa.Name = "btnXoa"
        Me.btnXoa.Size = New System.Drawing.Size(75, 23)
        Me.btnXoa.TabIndex = 12
        Me.btnXoa.Text = "Xóa"
        Me.btnXoa.UseVisualStyleBackColor = True
        '
        'btnCapNhat
        '
        Me.btnCapNhat.Location = New System.Drawing.Point(727, 110)
        Me.btnCapNhat.Name = "btnCapNhat"
        Me.btnCapNhat.Size = New System.Drawing.Size(75, 23)
        Me.btnCapNhat.TabIndex = 13
        Me.btnCapNhat.Text = "Cập nhật"
        Me.btnCapNhat.UseVisualStyleBackColor = True
        '
        'btnThoat
        '
        Me.btnThoat.Location = New System.Drawing.Point(727, 235)
        Me.btnThoat.Name = "btnThoat"
        Me.btnThoat.Size = New System.Drawing.Size(75, 23)
        Me.btnThoat.TabIndex = 15
        Me.btnThoat.Text = "Thoát"
        Me.btnThoat.UseVisualStyleBackColor = True
        '
        'btnLuu
        '
        Me.btnLuu.Location = New System.Drawing.Point(727, 177)
        Me.btnLuu.Name = "btnLuu"
        Me.btnLuu.Size = New System.Drawing.Size(75, 23)
        Me.btnLuu.TabIndex = 16
        Me.btnLuu.Text = "Lưu"
        Me.btnLuu.UseVisualStyleBackColor = True
        '
        'btnHuy
        '
        Me.btnHuy.Location = New System.Drawing.Point(727, 206)
        Me.btnHuy.Name = "btnHuy"
        Me.btnHuy.Size = New System.Drawing.Size(75, 23)
        Me.btnHuy.TabIndex = 17
        Me.btnHuy.Text = "Hủy"
        Me.btnHuy.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(321, 393)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(49, 13)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Tìm kiếm"
        '
        'txtTimKiem
        '
        Me.txtTimKiem.Location = New System.Drawing.Point(387, 390)
        Me.txtTimKiem.Name = "txtTimKiem"
        Me.txtTimKiem.Size = New System.Drawing.Size(262, 20)
        Me.txtTimKiem.TabIndex = 19
        '
        'btnTim
        '
        Me.btnTim.Location = New System.Drawing.Point(664, 389)
        Me.btnTim.Name = "btnTim"
        Me.btnTim.Size = New System.Drawing.Size(75, 23)
        Me.btnTim.TabIndex = 20
        Me.btnTim.Text = "Tìm"
        Me.btnTim.UseVisualStyleBackColor = True
        '
        'QuanLiTaiLieu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1043, 601)
        Me.Controls.Add(Me.btnTim)
        Me.Controls.Add(Me.txtTimKiem)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.btnHuy)
        Me.Controls.Add(Me.btnLuu)
        Me.Controls.Add(Me.btnThoat)
        Me.Controls.Add(Me.btnCapNhat)
        Me.Controls.Add(Me.btnXoa)
        Me.Controls.Add(Me.btnThem)
        Me.Controls.Add(Me.groupInfor)
        Me.Controls.Add(Me.dgvTaiLieu)
        Me.Controls.Add(Me.Label11)
        Me.Name = "QuanLiTaiLieu"
        Me.Text = "Quản lí tài liệu"
        CType(Me.dgvTaiLieu, System.ComponentModel.ISupportInitialize).EndInit()
        Me.groupInfor.ResumeLayout(False)
        Me.groupInfor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents dgvTaiLieu As System.Windows.Forms.DataGridView
    Friend WithEvents groupInfor As GroupBox
    Friend WithEvents txtTheLoai As TextBox
    Friend WithEvents txtNXB As TextBox
    Friend WithEvents txtTenTL As TextBox
    Friend WithEvents txtTacGia As TextBox
    Friend WithEvents txtTinhTrang As TextBox
    Friend WithEvents txtSL As TextBox
    Friend WithEvents txtGia As TextBox
    Friend WithEvents txtNamXB As TextBox
    Friend WithEvents txtMaTL As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btnThem As Button
    Friend WithEvents btnXoa As Button
    Friend WithEvents btnCapNhat As Button
    Friend WithEvents btnThoat As Button
    Friend WithEvents btnLuu As System.Windows.Forms.Button
    Friend WithEvents btnHuy As System.Windows.Forms.Button
    Friend WithEvents chkTLDB As System.Windows.Forms.CheckBox
    Friend WithEvents maTL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tenTL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tacgia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents theloai As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nhaxuatban As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents namXB As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SLuong As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tinhtrang As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TLDB As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtTimKiem As System.Windows.Forms.TextBox
    Friend WithEvents btnTim As System.Windows.Forms.Button
End Class
