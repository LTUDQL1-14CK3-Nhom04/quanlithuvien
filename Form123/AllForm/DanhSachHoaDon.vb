﻿Imports BUS
Imports DTO
Public Class DanhSachHoaDon
    Dim lst As New List(Of DTO.ChiTietHoaDon)
    Dim hdBus As New ChiTietHoaDonBUS()
    Private Sub DanhSachHoaDon_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lst = hdBus.getAll()
        Dim tbl = New DataTable()
        tbl.Columns.Add("maHD")
        tbl.Columns.Add("maDG")
        tbl.Columns.Add("tenDG")
        tbl.Columns.Add("loaiDG")
        tbl.Columns.Add("phithuongnien")
        tbl.Columns.Add("phiphat")
        tbl.Columns.Add("thanhtien")
        tbl.Columns.Add("ngaythu")
        For Each item In lst
            tbl.Rows.Add(item.maHD, item.maDG, item.DocGia.tenDG, item.DocGia.LoaiDG1.tenloai, item.PhiThuongNien, item.PhiPhat, item.ThanhTien, item.NgayThu)
        Next
        dgvHD.DataSource = tbl
    End Sub
End Class