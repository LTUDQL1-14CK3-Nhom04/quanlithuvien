﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PhieuMuon
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtMaPhieuMuon = New System.Windows.Forms.TextBox()
        Me.dgvMuon = New System.Windows.Forms.DataGridView()
        Me.mamuon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.madocgia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tenDG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.maTL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TenTL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ngaymuon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hanmuon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.groupInfor = New System.Windows.Forms.GroupBox()
        Me.dtHanMuon = New System.Windows.Forms.DateTimePicker()
        Me.dtNgayMuon = New System.Windows.Forms.DateTimePicker()
        Me.cbbMaTL = New System.Windows.Forms.ComboBox()
        Me.cbbMaDG = New System.Windows.Forms.ComboBox()
        Me.txtTenTL = New System.Windows.Forms.TextBox()
        Me.txtTenDG = New System.Windows.Forms.TextBox()
        Me.btnHuy = New System.Windows.Forms.Button()
        Me.btnLuu = New System.Windows.Forms.Button()
        Me.btnThoat = New System.Windows.Forms.Button()
        Me.btnCapNhat = New System.Windows.Forms.Button()
        Me.btnXoa = New System.Windows.Forms.Button()
        Me.btnThem = New System.Windows.Forms.Button()
        CType(Me.dgvMuon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.groupInfor.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label1.Location = New System.Drawing.Point(314, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(188, 31)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "PHIẾU MƯỢN"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Mã phiếu mượn"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 153)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Ngày mượn"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 106)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Mã tài liệu"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 71)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Mã độc giả"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 202)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Hạn mượn"
        '
        'txtMaPhieuMuon
        '
        Me.txtMaPhieuMuon.Location = New System.Drawing.Point(117, 28)
        Me.txtMaPhieuMuon.Name = "txtMaPhieuMuon"
        Me.txtMaPhieuMuon.Size = New System.Drawing.Size(152, 20)
        Me.txtMaPhieuMuon.TabIndex = 11
        '
        'dgvMuon
        '
        Me.dgvMuon.AllowUserToAddRows = False
        Me.dgvMuon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMuon.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.mamuon, Me.madocgia, Me.tenDG, Me.maTL, Me.TenTL, Me.ngaymuon, Me.hanmuon})
        Me.dgvMuon.Location = New System.Drawing.Point(52, 334)
        Me.dgvMuon.Name = "dgvMuon"
        Me.dgvMuon.Size = New System.Drawing.Size(741, 150)
        Me.dgvMuon.TabIndex = 47
        '
        'mamuon
        '
        Me.mamuon.DataPropertyName = "mamuon"
        Me.mamuon.HeaderText = "Mã phiếu mượn"
        Me.mamuon.Name = "mamuon"
        '
        'madocgia
        '
        Me.madocgia.DataPropertyName = "madocgia"
        Me.madocgia.HeaderText = "Mã độc giả"
        Me.madocgia.Name = "madocgia"
        '
        'tenDG
        '
        Me.tenDG.DataPropertyName = "tendocgia"
        Me.tenDG.HeaderText = "Tên độc giả"
        Me.tenDG.Name = "tenDG"
        '
        'maTL
        '
        Me.maTL.DataPropertyName = "maTL"
        Me.maTL.HeaderText = "Mã tài liệu"
        Me.maTL.Name = "maTL"
        '
        'TenTL
        '
        Me.TenTL.DataPropertyName = "tenTL"
        Me.TenTL.HeaderText = "Tên tài liệu"
        Me.TenTL.Name = "TenTL"
        '
        'ngaymuon
        '
        Me.ngaymuon.DataPropertyName = "ngaymuon"
        Me.ngaymuon.HeaderText = "Ngày mượn"
        Me.ngaymuon.Name = "ngaymuon"
        '
        'hanmuon
        '
        Me.hanmuon.DataPropertyName = "hanmuon"
        Me.hanmuon.HeaderText = "Hạn mượn"
        Me.hanmuon.Name = "hanmuon"
        '
        'groupInfor
        '
        Me.groupInfor.Controls.Add(Me.dtHanMuon)
        Me.groupInfor.Controls.Add(Me.dtNgayMuon)
        Me.groupInfor.Controls.Add(Me.cbbMaTL)
        Me.groupInfor.Controls.Add(Me.cbbMaDG)
        Me.groupInfor.Controls.Add(Me.txtMaPhieuMuon)
        Me.groupInfor.Controls.Add(Me.Label2)
        Me.groupInfor.Controls.Add(Me.Label3)
        Me.groupInfor.Controls.Add(Me.Label4)
        Me.groupInfor.Controls.Add(Me.Label5)
        Me.groupInfor.Controls.Add(Me.Label6)
        Me.groupInfor.Controls.Add(Me.txtTenTL)
        Me.groupInfor.Controls.Add(Me.txtTenDG)
        Me.groupInfor.Location = New System.Drawing.Point(109, 43)
        Me.groupInfor.Name = "groupInfor"
        Me.groupInfor.Size = New System.Drawing.Size(450, 285)
        Me.groupInfor.TabIndex = 48
        Me.groupInfor.TabStop = False
        Me.groupInfor.Text = "Thông tin mượn"
        '
        'dtHanMuon
        '
        Me.dtHanMuon.CustomFormat = "dd/MM/yyyy"
        Me.dtHanMuon.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtHanMuon.Location = New System.Drawing.Point(117, 196)
        Me.dtHanMuon.Name = "dtHanMuon"
        Me.dtHanMuon.Size = New System.Drawing.Size(152, 20)
        Me.dtHanMuon.TabIndex = 20
        '
        'dtNgayMuon
        '
        Me.dtNgayMuon.CustomFormat = "dd/MM/yyyy"
        Me.dtNgayMuon.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtNgayMuon.Location = New System.Drawing.Point(117, 153)
        Me.dtNgayMuon.Name = "dtNgayMuon"
        Me.dtNgayMuon.Size = New System.Drawing.Size(152, 20)
        Me.dtNgayMuon.TabIndex = 19
        '
        'cbbMaTL
        '
        Me.cbbMaTL.FormattingEnabled = True
        Me.cbbMaTL.Location = New System.Drawing.Point(117, 99)
        Me.cbbMaTL.Name = "cbbMaTL"
        Me.cbbMaTL.Size = New System.Drawing.Size(152, 21)
        Me.cbbMaTL.TabIndex = 18
        '
        'cbbMaDG
        '
        Me.cbbMaDG.FormattingEnabled = True
        Me.cbbMaDG.Location = New System.Drawing.Point(117, 64)
        Me.cbbMaDG.Name = "cbbMaDG"
        Me.cbbMaDG.Size = New System.Drawing.Size(152, 21)
        Me.cbbMaDG.TabIndex = 18
        '
        'txtTenTL
        '
        Me.txtTenTL.Enabled = False
        Me.txtTenTL.Location = New System.Drawing.Point(275, 99)
        Me.txtTenTL.Name = "txtTenTL"
        Me.txtTenTL.Size = New System.Drawing.Size(152, 20)
        Me.txtTenTL.TabIndex = 17
        '
        'txtTenDG
        '
        Me.txtTenDG.Enabled = False
        Me.txtTenDG.Location = New System.Drawing.Point(275, 64)
        Me.txtTenDG.Name = "txtTenDG"
        Me.txtTenDG.Size = New System.Drawing.Size(152, 20)
        Me.txtTenDG.TabIndex = 17
        '
        'btnHuy
        '
        Me.btnHuy.Location = New System.Drawing.Point(618, 201)
        Me.btnHuy.Name = "btnHuy"
        Me.btnHuy.Size = New System.Drawing.Size(75, 23)
        Me.btnHuy.TabIndex = 26
        Me.btnHuy.Text = "Hủy"
        Me.btnHuy.UseVisualStyleBackColor = True
        '
        'btnLuu
        '
        Me.btnLuu.Location = New System.Drawing.Point(618, 172)
        Me.btnLuu.Name = "btnLuu"
        Me.btnLuu.Size = New System.Drawing.Size(75, 23)
        Me.btnLuu.TabIndex = 25
        Me.btnLuu.Text = "Lưu"
        Me.btnLuu.UseVisualStyleBackColor = True
        '
        'btnThoat
        '
        Me.btnThoat.Location = New System.Drawing.Point(618, 245)
        Me.btnThoat.Name = "btnThoat"
        Me.btnThoat.Size = New System.Drawing.Size(75, 23)
        Me.btnThoat.TabIndex = 24
        Me.btnThoat.Text = "Thoát"
        Me.btnThoat.UseVisualStyleBackColor = True
        '
        'btnCapNhat
        '
        Me.btnCapNhat.Location = New System.Drawing.Point(618, 105)
        Me.btnCapNhat.Name = "btnCapNhat"
        Me.btnCapNhat.Size = New System.Drawing.Size(75, 23)
        Me.btnCapNhat.TabIndex = 22
        Me.btnCapNhat.Text = "Cập nhật"
        Me.btnCapNhat.UseVisualStyleBackColor = True
        '
        'btnXoa
        '
        Me.btnXoa.Location = New System.Drawing.Point(618, 140)
        Me.btnXoa.Name = "btnXoa"
        Me.btnXoa.Size = New System.Drawing.Size(75, 23)
        Me.btnXoa.TabIndex = 21
        Me.btnXoa.Text = "Xóa"
        Me.btnXoa.UseVisualStyleBackColor = True
        '
        'btnThem
        '
        Me.btnThem.Location = New System.Drawing.Point(618, 71)
        Me.btnThem.Name = "btnThem"
        Me.btnThem.Size = New System.Drawing.Size(75, 23)
        Me.btnThem.TabIndex = 20
        Me.btnThem.Text = "Thêm"
        Me.btnThem.UseVisualStyleBackColor = True
        '
        'PhieuMuon
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(901, 507)
        Me.Controls.Add(Me.btnHuy)
        Me.Controls.Add(Me.groupInfor)
        Me.Controls.Add(Me.btnLuu)
        Me.Controls.Add(Me.dgvMuon)
        Me.Controls.Add(Me.btnThoat)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnThem)
        Me.Controls.Add(Me.btnCapNhat)
        Me.Controls.Add(Me.btnXoa)
        Me.Name = "PhieuMuon"
        Me.Text = "Quản lí mượn"
        CType(Me.dgvMuon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.groupInfor.ResumeLayout(False)
        Me.groupInfor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtMaPhieuMuon As System.Windows.Forms.TextBox
    Friend WithEvents dgvMuon As System.Windows.Forms.DataGridView
    Friend WithEvents groupInfor As System.Windows.Forms.GroupBox
    Friend WithEvents cbbMaDG As System.Windows.Forms.ComboBox
    Friend WithEvents txtTenDG As System.Windows.Forms.TextBox
    Friend WithEvents cbbMaTL As System.Windows.Forms.ComboBox
    Friend WithEvents txtTenTL As System.Windows.Forms.TextBox
    Friend WithEvents dtNgayMuon As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnHuy As System.Windows.Forms.Button
    Friend WithEvents btnLuu As System.Windows.Forms.Button
    Friend WithEvents btnThoat As System.Windows.Forms.Button
    Friend WithEvents btnCapNhat As System.Windows.Forms.Button
    Friend WithEvents btnXoa As System.Windows.Forms.Button
    Friend WithEvents btnThem As System.Windows.Forms.Button
    Friend WithEvents dtHanMuon As System.Windows.Forms.DateTimePicker
    Friend WithEvents mamuon As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents madocgia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tenDG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents maTL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TenTL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ngaymuon As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hanmuon As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
