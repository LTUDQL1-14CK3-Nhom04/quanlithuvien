﻿Public Class TrangChu

    Private Sub TrangChu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        
    End Sub

    Private Sub ĐăngNhậpToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles dangnhapMenu.Click
        Dim frm As New TrangDangNhap()
        frm.ShowDialog()
        If (frm.DialogResult = Windows.Forms.DialogResult.OK) Then
            quanlyMenu.Enabled = True
            dangnhapMenu.Enabled = False
            dangxuatMenu.Enabled = True
        End If
    End Sub

    Private Sub QuảnLýĐộcGiảToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuảnLýĐộcGiảToolStripMenuItem.Click
        Dim frm As New QuanLiDocGia()
        frm.ShowDialog()
    End Sub

    Private Sub QuảnLýTàiLiệuToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuảnLýTàiLiệuToolStripMenuItem.Click
        Dim frm As New QuanLiTaiLieu()
        frm.ShowDialog()
    End Sub

    Private Sub QuảnLýMượnToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuảnLýMượnToolStripMenuItem.Click
        Dim frm = New PhieuMuon()
        frm.ShowDialog()
    End Sub

    Private Sub QuảnLýTrảToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuảnLýTrảToolStripMenuItem.Click
        Dim frm = New PhieuTra()
        frm.ShowDialog()
    End Sub

    Private Sub dangxuatMenu_Click(sender As Object, e As EventArgs) Handles dangxuatMenu.Click
        quanlyMenu.Enabled = False
        dangnhapMenu.Enabled = True
        dangxuatMenu.Enabled = False
    End Sub

    Private Sub ThoátToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ThoátToolStripMenuItem.Click
        Me.Dispose()
    End Sub

    Private Sub DanhSáchHóaĐơnToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DanhSáchHóaĐơnToolStripMenuItem.Click
        Dim frm = New DanhSachHoaDon()
        frm.ShowDialog()
    End Sub
End Class