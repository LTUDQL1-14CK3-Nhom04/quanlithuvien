﻿
Imports BUS
Imports DTO
Public Class QuanLiDocGia
    Dim them = False
    Dim sua = False

    Dim cnBUS As New DocGiaBUS()
    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub Form5_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbLoaiDG.DisplayMember = "tenloai"
        cbLoaiDG.ValueMember = "maloai"
        cbLoaiDG.DataSource = cnBUS.loadCBLoaiDG()
        cbbTimTheo.SelectedIndex = 0
        loaddata()

    End Sub
    Private Sub loaddata()
        them = False
        sua = False

        groupInfor.Enabled = False
        txtMaDG.Enabled = False

        btDangKi.Enabled = True


        btThuPhi.Enabled = True


        btLuu.Enabled = False
        btHuy.Enabled = False
        Dim dtb As New DataTable

        dgvDocGia.DataSource = dtb
        dgvDocGia.DataSource = cnBUS.loadDgvDocGia()
        If (dgvDocGia.Rows.Count > 0) Then
            btCapNhat.Enabled = True
            btXoa.Enabled = True
            btThuPhi.Enabled = True
            dgvDocGia_CellClick(Nothing, Nothing)
        Else
            btCapNhat.Enabled = False
            btXoa.Enabled = False
            btThuPhi.Enabled = False
        End If
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtMaDG.TextChanged

    End Sub

    Private Sub dgvDocGia_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDocGia.CellContentClick

        'txtSoSachQuaHan.Text = dgvDocGia.Rows(t).Cells(8).Value.ToString()

    End Sub

    Private Sub btLuu_Click(sender As Object, e As EventArgs) Handles btLuu.Click
        If (them) Then
            Try
                cnBUS.themDG(txtMaDG.Text, txtTenDG.Text, cbLoaiDG.SelectedValue, txtCMND.Text, txtSDT.Text, txtDiaChi.Text)
                MessageBox.Show("Thêm thành công!")
                loaddata()
            Catch ex As Exception

                MessageBox.Show("Không thêm được!" + ex.Message)
            End Try
        End If
        If (sua) Then
            Try
                cnBUS.suaDG(txtMaDG.Text, txtTenDG.Text, cbLoaiDG.SelectedValue, txtCMND.Text, txtSDT.Text, txtDiaChi.Text)
                MessageBox.Show("Sửa thành công!")
                loaddata()
            Catch ex As Exception

                MessageBox.Show("Không sửa được!" + ex.Message)
            End Try
        End If
    End Sub

    Private Sub btDangKi_Click(sender As Object, e As EventArgs) Handles btDangKi.Click
        them = True
        groupInfor.Enabled = True
        txtMaDG.Enabled = True

        btDangKi.Enabled = False
        btCapNhat.Enabled = False
        btXoa.Enabled = False
        btThuPhi.Enabled = False

        txtMaDG.Text = ""
        txtTenDG.Text = ""
        txtCMND.Text = ""
        txtSDT.Text = ""
        txtDiaChi.Text = ""
        cbLoaiDG.Text = ""
        txtSoSachMuonToiDa.Text = ""

        btLuu.Enabled = True
        btHuy.Enabled = True
    End Sub

    Private Sub btCapNhat_Click(sender As Object, e As EventArgs) Handles btCapNhat.Click
        sua = True

        groupInfor.Enabled = True
        txtMaDG.Enabled = False

        btDangKi.Enabled = False
        btCapNhat.Enabled = False
        btXoa.Enabled = False
        btThuPhi.Enabled = False


        btLuu.Enabled = True
        btHuy.Enabled = True
    End Sub

    Private Sub btHuy_Click(sender As Object, e As EventArgs) Handles btHuy.Click
        loaddata()

    End Sub

    Private Sub cbLoaiDG_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbLoaiDG.SelectedIndexChanged

    End Sub

    Private Sub dgvDocGia_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDocGia.CellClick
        Dim t As Integer = dgvDocGia.CurrentCell.RowIndex

        txtMaDG.Text = dgvDocGia.Rows(t).Cells("maDG").Value.ToString()
        txtTenDG.Text = dgvDocGia.Rows(t).Cells("tenDG").Value.ToString()
        txtCMND.Text = dgvDocGia.Rows(t).Cells("CMND").Value.ToString()
        txtSDT.Text = dgvDocGia.Rows(t).Cells("SDT").Value.ToString()
        txtDiaChi.Text = dgvDocGia.Rows(t).Cells("DiaChi").Value.ToString()
        cbLoaiDG.Text = dgvDocGia.Rows(t).Cells("tenloai").Value.ToString()
        txtSoSachMuonToiDa.Text = dgvDocGia.Rows(t).Cells("SLmuontoida").Value.ToString()
    End Sub

    Private Sub btXoa_Click(sender As Object, e As EventArgs) Handles btXoa.Click
        Dim result = MessageBox.Show("Bạn có muốn xóa không?", "Hỏi?", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If (result = DialogResult.Yes) Then
            Dim t As Integer = dgvDocGia.CurrentCell.RowIndex
            Dim maDg = Integer.Parse(dgvDocGia.Rows(t).Cells("maDG").Value.ToString())
            Try
                cnBUS.xoaDG(maDg)
                MessageBox.Show("Xóa thành công!")
                loaddata()
            Catch ex As Exception
                MessageBox.Show("Không xóa được!" + ex.Message)
            End Try
        End If

    End Sub

    Private Sub btnTim_Click(sender As Object, e As EventArgs) Handles btnTim.Click
        If (cbbTimTheo.SelectedIndex = 0) Then
            Try
                Dim ma = Integer.Parse(txtTim.Text)

                dgvDocGia.DataSource = cnBUS.TimDocGiaTheoMa(ma)
            Catch ex As Exception
                MessageBox.Show("Mã độc giả là số!")
                Return
            End Try
        End If
        If (cbbTimTheo.SelectedIndex = 1) Then
            dgvDocGia.DataSource = cnBUS.TimDocGiaTheoTen(txtTim.Text)
        End If
        If (cbbTimTheo.SelectedIndex = 2) Then
            dgvDocGia.DataSource = cnBUS.TimDocGiaTheoCMND(txtTim.Text)
        End If
        If (dgvDocGia.Rows.Count > 0) Then
            btCapNhat.Enabled = True
            btXoa.Enabled = True
            btThuPhi.Enabled = True
            dgvDocGia_CellClick(Nothing, Nothing)
        Else
            btCapNhat.Enabled = False
            btXoa.Enabled = False
            btThuPhi.Enabled = False
        End If
    End Sub

    Private Sub btThuPhi_Click(sender As Object, e As EventArgs) Handles btThuPhi.Click
        Dim thuphi = New ThuPhi()
        thuphi.maDG = txtMaDG.Text
        thuphi.ShowDialog()

    End Sub
End Class