﻿Imports DTO
Imports System.Data
Imports System.Data.SqlClient

Public Class DocGiaDAO

    Public Function XemDanhSachDocGia() As DataTable
        Dim conn = DataProvider.connectDB()
        Dim str = "select * from DocGia"
        Dim dt As New DataTable()
        Dim da As New SqlDataAdapter(str, conn)
        da.Fill(dt)
        Return dt
    End Function
    Public Function GetAll() As List(Of DocGia)
        Dim dbs = New DALDataContext()
        Return dbs.DocGias.ToList()
    End Function
    Public Function TimDocGiaTheoMa(maDG As Integer) As DataTable
        Dim conn = DataProvider.connectDB()
        Dim str = String.Format("select maDG, tenDG, L.tenloai, CMND, SDT, diachi, SLmuontoida from DocGia DG, LoaiDG L WHERE DG.loaiDG=L.maloai and maDG={0}", maDG)
        Dim dt As New DataTable()
        Dim da As New SqlDataAdapter(str, conn)
        da.Fill(dt)
        Return dt
    End Function
    Public Function TimDocGiaTheoTen(tenDG As String) As DataTable
        Dim conn = DataProvider.connectDB()
        Dim str = String.Format("select maDG, tenDG, L.tenloai, CMND, SDT, diachi, SLmuontoida from DocGia DG, LoaiDG L WHERE DG.loaiDG=L.maloai and tenDG like N'%{0}%'", tenDG)
        Dim dt As New DataTable()
        Dim da As New SqlDataAdapter(str, conn)
        da.Fill(dt)
        Return dt
    End Function
    Public Function TimDocGiaTheoCMND(cmnd As String) As DataTable
        Dim conn = DataProvider.connectDB()
        Dim str = String.Format("select maDG, tenDG, L.tenloai, CMND, SDT, diachi, SLmuontoida from DocGia DG, LoaiDG L WHERE DG.loaiDG=L.maloai and CMND like '%{0}%'", cmnd)
        Dim dt As New DataTable()
        Dim da As New SqlDataAdapter(str, conn)
        da.Fill(dt)
        Return dt
    End Function
    Public Function loadCBLoaiDG() As DataTable
        Dim str = String.Format("select * from LoaiDG")
        Dim conn = DataProvider.connectDB()
        Dim adapter As New SqlDataAdapter(str, conn)
        Dim table As New DataTable()
        adapter.Fill(table)
        conn.Close()
        Return table
    End Function
    Public Function loaddgv() As DataTable
        Dim str = String.Format("select maDG, tenDG, L.tenloai, CMND, SDT, diachi, SLmuontoida from DocGia DG, LoaiDG L WHERE DG.loaiDG=L.maloai")
        Dim conn = DataProvider.connectDB()
        Dim adapter As New SqlDataAdapter (str, conn)
        Dim table As New DataTable()
        adapter.Fill(table)
        conn.Close()
        Return table
    End Function
    Public Function themDG(maDG As Integer, tenDG As String, loaiDG As Integer, CMND As String, SDT As String, diachi As String) As Boolean

        Dim str = String.Format("insert into DocGia(maDG, tenDG, loaiDG, CMND, SDT, diachi) values({0}, N'{1}',{2},N'{3}', N'{4}',N'{5}')", maDG, tenDG, loaiDG, CMND, SDT, diachi)
            Dim conn = DataProvider.connectDB()
            Dim command As New SqlCommand(str, conn)
            command.ExecuteNonQuery()
            conn.Close()
            Return True

    End Function
    Public Function suaDG(maDG As Integer, tenDG As String, loaiDG As Integer, CMND As String, SDT As String, diachi As String) As Boolean

        Dim sql = String.Format("update DocGia set tenDG =N'{1}', loaiDG={2}, CMND=N'{3}', SDT=N'{4}', diachi=N'{5}' where maDG={0}", maDG, tenDG, loaiDG, CMND, SDT, diachi)
        Dim conn = DataProvider.connectDB()
        Dim command As New SqlCommand(sql, conn)
        command.ExecuteNonQuery()
        conn.Close()
        Return True
    End Function
    Public Function xoaDG(maDG As Integer) As Boolean
        Dim sql = String.Format("delete from DocGia where maDG={0}", maDG)
        Dim conn = DataProvider.connectDB()
        Dim command As New SqlCommand(sql, conn)
        command.ExecuteNonQuery()
        conn.Close()
        Return True
    End Function
End Class
