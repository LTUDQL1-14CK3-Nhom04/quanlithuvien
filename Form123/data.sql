USE [master]
GO
/****** Object:  Database [QLThuVien]    Script Date: 1/12/2017 11:08:54 AM ******/
CREATE DATABASE [QLThuVien]
 GO
ALTER DATABASE [QLThuVien] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLThuVien].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLThuVien] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLThuVien] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLThuVien] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLThuVien] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLThuVien] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLThuVien] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QLThuVien] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLThuVien] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLThuVien] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLThuVien] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLThuVien] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLThuVien] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLThuVien] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLThuVien] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLThuVien] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QLThuVien] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLThuVien] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLThuVien] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLThuVien] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLThuVien] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLThuVien] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLThuVien] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLThuVien] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QLThuVien] SET  MULTI_USER 
GO
ALTER DATABASE [QLThuVien] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLThuVien] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QLThuVien] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QLThuVien] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [QLThuVien] SET DELAYED_DURABILITY = DISABLED 
GO
USE [QLThuVien]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 1/12/2017 11:08:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[username] [nvarchar](10) NOT NULL,
	[pass] [nchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[username] ASC,
	[pass] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChiTietHoaDon]    Script Date: 1/12/2017 11:08:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietHoaDon](
	[maHD] [int] NOT NULL,
	[maDG] [int] NOT NULL,
	[PhiThuongNien] [float] NULL,
	[PhiPhat] [float] NULL,
	[ThanhTien] [float] NULL,
	[NgayThu] [nvarchar](20) NULL,
 CONSTRAINT [PK_ChiTietHoaDon_1] PRIMARY KEY CLUSTERED 
(
	[maHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DocGia]    Script Date: 1/12/2017 11:08:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocGia](
	[maDG] [int] NOT NULL,
	[tenDG] [nvarchar](50) NULL,
	[loaiDG] [int] NULL,
	[CMND] [varchar](9) NULL,
	[SDT] [nvarchar](12) NULL,
	[gioitinh] [nchar](3) NULL,
	[diachi] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[maDG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiDG]    Script Date: 1/12/2017 11:08:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiDG](
	[maloai] [int] NOT NULL,
	[tenloai] [nvarchar](20) NULL,
	[SLmuontoida] [int] NULL,
	[TLdacbiet] [bit] NULL,
	[songaymuontoida] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[maloai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Muon]    Script Date: 1/12/2017 11:08:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Muon](
	[mamuon] [int] NOT NULL,
	[madocgia] [int] NULL,
	[ngaymuon] [nvarchar](20) NULL,
	[hanmuon] [nvarchar](50) NULL,
	[maTL] [int] NULL,
 CONSTRAINT [PK__Muon__6824B04B92AF9244] PRIMARY KEY CLUSTERED 
(
	[mamuon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaiLieu]    Script Date: 1/12/2017 11:08:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaiLieu](
	[maTL] [int] NOT NULL,
	[tenTL] [nvarchar](50) NOT NULL,
	[TLDB] [bit] NULL,
	[tacgia] [nvarchar](35) NULL,
	[theloai] [nvarchar](20) NULL,
	[nhaxuatban] [nvarchar](50) NULL,
	[namXB] [int] NULL,
	[gia] [float] NULL,
	[SLuong] [int] NULL,
	[Tinhtrang] [nchar](3) NULL,
PRIMARY KEY CLUSTERED 
(
	[maTL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tra]    Script Date: 1/12/2017 11:08:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tra](
	[matra] [int] NOT NULL,
	[mamuon] [int] NULL,
	[ngaytra] [nvarchar](20) NULL,
 CONSTRAINT [PK__Tra__1489DFF49B08CE42] PRIMARY KEY CLUSTERED 
(
	[matra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Admin] ([username], [pass]) VALUES (N'0101010', N'DucNgo    ')
INSERT [dbo].[Admin] ([username], [pass]) VALUES (N'1460630', N'KimNgan   ')
INSERT [dbo].[Admin] ([username], [pass]) VALUES (N'1461101', N'NgocTuyet ')
INSERT [dbo].[Admin] ([username], [pass]) VALUES (N'1461405', N'ThiKhanh  ')
INSERT [dbo].[Admin] ([username], [pass]) VALUES (N'1461703', N'MaiTruc   ')
INSERT [dbo].[Admin] ([username], [pass]) VALUES (N'admin', N'admin     ')
INSERT [dbo].[ChiTietHoaDon] ([maHD], [maDG], [PhiThuongNien], [PhiPhat], [ThanhTien], [NgayThu]) VALUES (1, 1101, NULL, NULL, NULL, NULL)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [maDG], [PhiThuongNien], [PhiPhat], [ThanhTien], [NgayThu]) VALUES (2, 1102, NULL, NULL, NULL, NULL)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [maDG], [PhiThuongNien], [PhiPhat], [ThanhTien], [NgayThu]) VALUES (3, 1101, 432, 4324, 186624, N'11/01/2017')
INSERT [dbo].[ChiTietHoaDon] ([maHD], [maDG], [PhiThuongNien], [PhiPhat], [ThanhTien], [NgayThu]) VALUES (5, 1101, 1000000, 100000, 1010000, N'11/01/2017')
INSERT [dbo].[ChiTietHoaDon] ([maHD], [maDG], [PhiThuongNien], [PhiPhat], [ThanhTien], [NgayThu]) VALUES (14, 1105, 10000, 1000, 1000000, N'11/01/2017')
INSERT [dbo].[DocGia] ([maDG], [tenDG], [loaiDG], [CMND], [SDT], [gioitinh], [diachi]) VALUES (1101, N'Nguyễn Hoài An', 1001, N'264464242', N'01641234567', N'Nam', N'25/3 Lạc ng Quân, Q.10, TP HCM')
INSERT [dbo].[DocGia] ([maDG], [tenDG], [loaiDG], [CMND], [SDT], [gioitinh], [diachi]) VALUES (1102, N'Nguyễn Ngọc Ánh', 1001, N'26412560', N'01688415182', N'Nữ ', N'12/21 Võ Văn Ngân Thủ Đức, TP HCM')
INSERT [dbo].[DocGia] ([maDG], [tenDG], [loaiDG], [CMND], [SDT], [gioitinh], [diachi]) VALUES (1103, N'Trương Nam Sơn', 1002, N'264123264', N'01653673874', N'Nam', N'22/5 Nguyễn Xí, Q.Bình Thạnh, TPHCM')
INSERT [dbo].[DocGia] ([maDG], [tenDG], [loaiDG], [CMND], [SDT], [gioitinh], [diachi]) VALUES (1104, N'Trần Trung Hiếu', 1001, N'26466342', N'09146374631', N'Nam', N'234 Trấn não, An Phú, TPHCM')
INSERT [dbo].[DocGia] ([maDG], [tenDG], [loaiDG], [CMND], [SDT], [gioitinh], [diachi]) VALUES (1105, N'Trần Bạch Tuyết', 1002, N'26475244', N'01635425635', N'Nam', N'221 hùng Vương, Q.5, TPHCM')
INSERT [dbo].[LoaiDG] ([maloai], [tenloai], [SLmuontoida], [TLdacbiet], [songaymuontoida]) VALUES (1001, N'Vip', 5, 1, 14)
INSERT [dbo].[LoaiDG] ([maloai], [tenloai], [SLmuontoida], [TLdacbiet], [songaymuontoida]) VALUES (1002, N'Thường', 3, 0, 5)
INSERT [dbo].[Muon] ([mamuon], [madocgia], [ngaymuon], [hanmuon], [maTL]) VALUES (1, 1101, N'01/01/2017', N'30/01/2017', 3)
INSERT [dbo].[Muon] ([mamuon], [madocgia], [ngaymuon], [hanmuon], [maTL]) VALUES (2, 1104, N'01/01/2017', N'23/01/2017', 1)
INSERT [dbo].[Muon] ([mamuon], [madocgia], [ngaymuon], [hanmuon], [maTL]) VALUES (3, 1105, N'01/01/2017', N'30/01/2017', 3)
INSERT [dbo].[TaiLieu] ([maTL], [tenTL], [TLDB], [tacgia], [theloai], [nhaxuatban], [namXB], [gia], [SLuong], [Tinhtrang]) VALUES (1, N'Code Complete 2', 1, N'Steve McConnell', N'CNTT', N'Microsoft', 2004, 873181, 3, N'còn')
INSERT [dbo].[TaiLieu] ([maTL], [tenTL], [TLDB], [tacgia], [theloai], [nhaxuatban], [namXB], [gia], [SLuong], [Tinhtrang]) VALUES (2, N'Học Nhanh Đồ Họa', 0, N'Không rõ', N'CNTT', N'Văn Hóa Thông Tin', 2010, 229000, 10, N'còn')
INSERT [dbo].[TaiLieu] ([maTL], [tenTL], [TLDB], [tacgia], [theloai], [nhaxuatban], [namXB], [gia], [SLuong], [Tinhtrang]) VALUES (3, N'Những Cánh Thư Chưa Khép', 0, N'Jenny Han', N'Báo sinh viên', N'NXB Thanh Niên', 2016, 75000, 5, N'còn')
INSERT [dbo].[TaiLieu] ([maTL], [tenTL], [TLDB], [tacgia], [theloai], [nhaxuatban], [namXB], [gia], [SLuong], [Tinhtrang]) VALUES (4, N'Nhật Bản Có Điều Kỳ Diệu', 0, N'Nhiều Tác Giả', N'Hồi kí', N'Báo Sinh Viên VN - Hoa Học Trò', 2015, 53000, 10, N'hết')
INSERT [dbo].[TaiLieu] ([maTL], [tenTL], [TLDB], [tacgia], [theloai], [nhaxuatban], [namXB], [gia], [SLuong], [Tinhtrang]) VALUES (5, N'Einstein - Cuộc Đời Và Vũ Trụ', 1, N'Walter Isaacson', N'CNTT', N'NXB Th? Gi?i', 2016, 239000, 2, N'còn')
INSERT [dbo].[Tra] ([matra], [mamuon], [ngaytra]) VALUES (2, 1, N'11/01/2017')
INSERT [dbo].[Tra] ([matra], [mamuon], [ngaytra]) VALUES (3, 2, N'19/01/2017')
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_DocGia] FOREIGN KEY([maDG])
REFERENCES [dbo].[DocGia] ([maDG])
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_DocGia]
GO
ALTER TABLE [dbo].[DocGia]  WITH CHECK ADD  CONSTRAINT [FK_DG_ML1] FOREIGN KEY([loaiDG])
REFERENCES [dbo].[LoaiDG] ([maloai])
GO
ALTER TABLE [dbo].[DocGia] CHECK CONSTRAINT [FK_DG_ML1]
GO
ALTER TABLE [dbo].[Muon]  WITH CHECK ADD  CONSTRAINT [FK_Muon_DG] FOREIGN KEY([madocgia])
REFERENCES [dbo].[DocGia] ([maDG])
GO
ALTER TABLE [dbo].[Muon] CHECK CONSTRAINT [FK_Muon_DG]
GO
ALTER TABLE [dbo].[Muon]  WITH CHECK ADD  CONSTRAINT [FK_Muon_TaiLieu] FOREIGN KEY([maTL])
REFERENCES [dbo].[TaiLieu] ([maTL])
GO
ALTER TABLE [dbo].[Muon] CHECK CONSTRAINT [FK_Muon_TaiLieu]
GO
ALTER TABLE [dbo].[Tra]  WITH CHECK ADD  CONSTRAINT [FK_Tra_Muon1] FOREIGN KEY([mamuon])
REFERENCES [dbo].[Muon] ([mamuon])
GO
ALTER TABLE [dbo].[Tra] CHECK CONSTRAINT [FK_Tra_Muon1]
GO
USE [master]
GO
ALTER DATABASE [QLThuVien] SET  READ_WRITE 
GO
