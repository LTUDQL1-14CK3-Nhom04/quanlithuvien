﻿Imports DTO
Imports BUS

Public Class QuanLiTaiLieu
    Private tlBus As New TaiLieuBUS()
    Private them As Boolean
    Private sua As Boolean
    Private lstTL As List(Of TaiLieu)

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTaiLieu.CellContentClick

    End Sub

    Private Sub QuanLiTaiLieu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loaddata()

    End Sub
    Private Sub loaddata()
        lstTL = tlBus.getAll
        dgvTaiLieu.DataSource = lstTL
        them = False
        sua = False

        groupInfor.Enabled = False
        txtMaTL.Enabled = False

        btnThem.Enabled = True
        btnXoa.Enabled = True
        btnCapNhat.Enabled = True

        btnLuu.Enabled = False
        btnHuy.Enabled = False

        If (dgvTaiLieu.Rows.Count > 0) Then
            btnCapNhat.Enabled = True
            btnXoa.Enabled = True
            dgvTaiLieu_CellClick(Nothing, Nothing)
        Else
            btnCapNhat.Enabled = False
            btnXoa.Enabled = False
        End If
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub dgvTaiLieu_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTaiLieu.CellClick
        Dim index = dgvTaiLieu.CurrentRow.Index
        Dim tl = lstTL(index)
        txtMaTL.Text = tl.maTL
        txtGia.Text = tl.gia
        txtNamXB.Text = tl.namXB
        txtNXB.Text = tl.nhaxuatban
        txtSL.Text = tl.SLuong.ToString()
        txtTacGia.Text = tl.tacgia
        txtTenTL.Text = tl.tenTL
        txtTheLoai.Text = tl.theloai
        txtTinhTrang.Text = tl.Tinhtrang
        chkTLDB.Checked = tl.TLDB
    End Sub

    Private Sub btnThoat_Click(sender As Object, e As EventArgs) Handles btnThoat.Click
        Me.Dispose()

    End Sub

    Private Sub btnThem_Click(sender As Object, e As EventArgs) Handles btnThem.Click
        them = True
        sua = False

        groupInfor.Enabled = True
        txtMaTL.Enabled = True

        btnThem.Enabled = False
        btnXoa.Enabled = False
        btnCapNhat.Enabled = False

        btnLuu.Enabled = True
        btnHuy.Enabled = True
        txtMaTL.Text = ""
        txtGia.Text = ""
        txtNamXB.Text = ""
        txtNXB.Text = ""
        txtSL.Text = ""
        txtTacGia.Text = ""
        txtTenTL.Text = ""
        txtTheLoai.Text = ""
        txtTinhTrang.Text = ""
        chkTLDB.Checked = False


    End Sub

    Private Sub btnCapNhat_Click(sender As Object, e As EventArgs) Handles btnCapNhat.Click
        them = False
        sua = True

        groupInfor.Enabled = True
        txtMaTL.Enabled = False

        btnThem.Enabled = False
        btnXoa.Enabled = False
        btnCapNhat.Enabled = False

        btnLuu.Enabled = True
        btnHuy.Enabled = True

    End Sub

    Private Sub btnXoa_Click(sender As Object, e As EventArgs) Handles btnXoa.Click
        Dim result = MessageBox.Show("Bạn có muốn xóa không?", "Hỏi?", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If (result = DialogResult.Yes) Then
            Dim t As Integer = dgvTaiLieu.CurrentCell.RowIndex

            Try
                Dim tl = New TaiLieu()
                tl.maTL = lstTL(t).maTL
                tlBus.xoa(tl)
                MessageBox.Show("Xóa thành công!")
                loaddata()
            Catch ex As Exception
                MessageBox.Show("Không xóa được!" + ex.Message)
            End Try
        End If
    End Sub

    Private Sub btnLuu_Click(sender As Object, e As EventArgs) Handles btnLuu.Click
        Dim tl = New TaiLieu()
        tl.maTL = txtMaTL.Text
        tl.gia = txtGia.Text
        tl.namXB = txtNamXB.Text
        tl.nhaxuatban = txtNXB.Text
        tl.SLuong = txtSL.Text
        tl.tacgia = txtTacGia.Text
        tl.theloai = txtTheLoai.Text
        tl.Tinhtrang = txtTinhTrang.Text
        tl.TLDB = chkTLDB.Checked
        tl.tenTL = txtTenTL.Text


        If (them) Then
            Try
                tlBus.them(tl)
                MessageBox.Show("Thêm thành công!")
                loaddata()

            Catch ex As Exception
                MessageBox.Show("Lỗi! Không thêm được! Lỗi: " + ex.Message)
            End Try
        End If
        If (sua) Then
            Try
                tlBus.sua(tl)
                MessageBox.Show("Sửa thành công!")
                loaddata()

            Catch ex As Exception
                MessageBox.Show("Lỗi! Không sửa được! Lỗi: " + ex.Message)
            End Try
        End If
    End Sub

    Private Sub btnHuy_Click(sender As Object, e As EventArgs) Handles btnHuy.Click
        loaddata()
    End Sub



    Private Sub btnTim_Click(sender As Object, e As EventArgs) Handles btnTim.Click
        lstTL = tlBus.tim(txtTimKiem.Text)
        dgvTaiLieu.DataSource = lstTL
        If (dgvTaiLieu.Rows.Count > 0) Then
            btnCapNhat.Enabled = True
            btnXoa.Enabled = True
            dgvTaiLieu_CellClick(Nothing, Nothing)
        Else
            btnCapNhat.Enabled = False
            btnXoa.Enabled = False
        End If
    End Sub
End Class