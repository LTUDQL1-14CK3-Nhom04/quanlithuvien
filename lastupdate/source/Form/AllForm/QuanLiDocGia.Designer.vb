﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class QuanLiDocGia
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.dgvDocGia = New System.Windows.Forms.DataGridView()
        Me.maDG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tenDG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CMND = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SDT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiaChi = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tenloai = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SLmuontoida = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.groupInfor = New System.Windows.Forms.GroupBox()
        Me.cbLoaiDG = New System.Windows.Forms.ComboBox()
        Me.txtDiaChi = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtSDT = New System.Windows.Forms.TextBox()
        Me.txtTenDG = New System.Windows.Forms.TextBox()
        Me.txtCMND = New System.Windows.Forms.TextBox()
        Me.txtSoSachQuaHan = New System.Windows.Forms.TextBox()
        Me.txtSoSachMuonToiDa = New System.Windows.Forms.TextBox()
        Me.txtMaDG = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.btDangKi = New System.Windows.Forms.Button()
        Me.btXoa = New System.Windows.Forms.Button()
        Me.btCapNhat = New System.Windows.Forms.Button()
        Me.btThuPhi = New System.Windows.Forms.Button()
        Me.btThoat = New System.Windows.Forms.Button()
        Me.btLuu = New System.Windows.Forms.Button()
        Me.btHuy = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cbbTimTheo = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtTim = New System.Windows.Forms.TextBox()
        Me.btnTim = New System.Windows.Forms.Button()
        CType(Me.dgvDocGia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.groupInfor.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvDocGia
        '
        Me.dgvDocGia.AllowUserToAddRows = False
        Me.dgvDocGia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDocGia.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.maDG, Me.tenDG, Me.CMND, Me.SDT, Me.DiaChi, Me.tenloai, Me.SLmuontoida})
        Me.dgvDocGia.Location = New System.Drawing.Point(12, 410)
        Me.dgvDocGia.Name = "dgvDocGia"
        Me.dgvDocGia.Size = New System.Drawing.Size(941, 143)
        Me.dgvDocGia.TabIndex = 20
        '
        'maDG
        '
        Me.maDG.DataPropertyName = "maDG"
        Me.maDG.HeaderText = "Mã độc giả"
        Me.maDG.Name = "maDG"
        '
        'tenDG
        '
        Me.tenDG.DataPropertyName = "tenDG"
        Me.tenDG.HeaderText = "Tên độc giả"
        Me.tenDG.Name = "tenDG"
        '
        'CMND
        '
        Me.CMND.DataPropertyName = "CMND"
        Me.CMND.HeaderText = "CMND"
        Me.CMND.Name = "CMND"
        '
        'SDT
        '
        Me.SDT.DataPropertyName = "SDT"
        Me.SDT.HeaderText = "Số điện thoại"
        Me.SDT.Name = "SDT"
        '
        'DiaChi
        '
        Me.DiaChi.DataPropertyName = "diachi"
        Me.DiaChi.HeaderText = "Địa chỉ"
        Me.DiaChi.Name = "DiaChi"
        '
        'tenloai
        '
        Me.tenloai.DataPropertyName = "tenloai"
        Me.tenloai.HeaderText = "Loại độc giả"
        Me.tenloai.Name = "tenloai"
        Me.tenloai.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'SLmuontoida
        '
        Me.SLmuontoida.DataPropertyName = "SLmuontoida"
        Me.SLmuontoida.HeaderText = "Số sách mượn tối đa"
        Me.SLmuontoida.Name = "SLmuontoida"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label1.Location = New System.Drawing.Point(304, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(262, 33)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "QUẢN LÍ ĐỘC GIẢ"
        '
        'groupInfor
        '
        Me.groupInfor.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.groupInfor.Controls.Add(Me.cbLoaiDG)
        Me.groupInfor.Controls.Add(Me.txtDiaChi)
        Me.groupInfor.Controls.Add(Me.Label3)
        Me.groupInfor.Controls.Add(Me.Label2)
        Me.groupInfor.Controls.Add(Me.txtSDT)
        Me.groupInfor.Controls.Add(Me.txtTenDG)
        Me.groupInfor.Controls.Add(Me.txtCMND)
        Me.groupInfor.Controls.Add(Me.txtSoSachQuaHan)
        Me.groupInfor.Controls.Add(Me.txtSoSachMuonToiDa)
        Me.groupInfor.Controls.Add(Me.txtMaDG)
        Me.groupInfor.Controls.Add(Me.Label9)
        Me.groupInfor.Controls.Add(Me.Label7)
        Me.groupInfor.Controls.Add(Me.Label6)
        Me.groupInfor.Controls.Add(Me.Label5)
        Me.groupInfor.Controls.Add(Me.Label4)
        Me.groupInfor.Controls.Add(Me.Label11)
        Me.groupInfor.Location = New System.Drawing.Point(149, 59)
        Me.groupInfor.Name = "groupInfor"
        Me.groupInfor.Size = New System.Drawing.Size(473, 274)
        Me.groupInfor.TabIndex = 26
        Me.groupInfor.TabStop = False
        Me.groupInfor.Text = "Thông tin độc giả"
        '
        'cbLoaiDG
        '
        Me.cbLoaiDG.FormattingEnabled = True
        Me.cbLoaiDG.Location = New System.Drawing.Point(149, 174)
        Me.cbLoaiDG.Name = "cbLoaiDG"
        Me.cbLoaiDG.Size = New System.Drawing.Size(268, 21)
        Me.cbLoaiDG.TabIndex = 23
        '
        'txtDiaChi
        '
        Me.txtDiaChi.Location = New System.Drawing.Point(149, 147)
        Me.txtDiaChi.Name = "txtDiaChi"
        Me.txtDiaChi.Size = New System.Drawing.Size(268, 20)
        Me.txtDiaChi.TabIndex = 22
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(21, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Số điện thoại"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 154)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Địa chỉ"
        '
        'txtSDT
        '
        Me.txtSDT.Location = New System.Drawing.Point(149, 121)
        Me.txtSDT.Name = "txtSDT"
        Me.txtSDT.Size = New System.Drawing.Size(268, 20)
        Me.txtSDT.TabIndex = 19
        '
        'txtTenDG
        '
        Me.txtTenDG.Location = New System.Drawing.Point(149, 59)
        Me.txtTenDG.Name = "txtTenDG"
        Me.txtTenDG.Size = New System.Drawing.Size(268, 20)
        Me.txtTenDG.TabIndex = 17
        '
        'txtCMND
        '
        Me.txtCMND.Location = New System.Drawing.Point(149, 88)
        Me.txtCMND.Name = "txtCMND"
        Me.txtCMND.Size = New System.Drawing.Size(268, 20)
        Me.txtCMND.TabIndex = 16
        '
        'txtSoSachQuaHan
        '
        Me.txtSoSachQuaHan.Enabled = False
        Me.txtSoSachQuaHan.Location = New System.Drawing.Point(149, 234)
        Me.txtSoSachQuaHan.Name = "txtSoSachQuaHan"
        Me.txtSoSachQuaHan.Size = New System.Drawing.Size(268, 20)
        Me.txtSoSachQuaHan.TabIndex = 12
        '
        'txtSoSachMuonToiDa
        '
        Me.txtSoSachMuonToiDa.Enabled = False
        Me.txtSoSachMuonToiDa.Location = New System.Drawing.Point(149, 203)
        Me.txtSoSachMuonToiDa.Name = "txtSoSachMuonToiDa"
        Me.txtSoSachMuonToiDa.Size = New System.Drawing.Size(268, 20)
        Me.txtSoSachMuonToiDa.TabIndex = 11
        '
        'txtMaDG
        '
        Me.txtMaDG.Location = New System.Drawing.Point(149, 30)
        Me.txtMaDG.Name = "txtMaDG"
        Me.txtMaDG.Size = New System.Drawing.Size(268, 20)
        Me.txtMaDG.TabIndex = 10
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(21, 66)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Tên độc giả"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(21, 210)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(105, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Số sách mượn tối đa"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(21, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "CMND"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(21, 184)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Loại độc giả"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(21, 237)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Số sách quá hạn"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(21, 37)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(61, 13)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Mã độc giả"
        '
        'btDangKi
        '
        Me.btDangKi.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btDangKi.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btDangKi.ForeColor = System.Drawing.SystemColors.Control
        Me.btDangKi.Location = New System.Drawing.Point(673, 54)
        Me.btDangKi.Name = "btDangKi"
        Me.btDangKi.Size = New System.Drawing.Size(75, 34)
        Me.btDangKi.TabIndex = 27
        Me.btDangKi.Text = "Đăng kí"
        Me.btDangKi.UseVisualStyleBackColor = False
        '
        'btXoa
        '
        Me.btXoa.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btXoa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btXoa.ForeColor = System.Drawing.SystemColors.Control
        Me.btXoa.Location = New System.Drawing.Point(673, 139)
        Me.btXoa.Name = "btXoa"
        Me.btXoa.Size = New System.Drawing.Size(75, 34)
        Me.btXoa.TabIndex = 28
        Me.btXoa.Text = "Xóa"
        Me.btXoa.UseVisualStyleBackColor = False
        '
        'btCapNhat
        '
        Me.btCapNhat.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btCapNhat.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btCapNhat.ForeColor = System.Drawing.SystemColors.Control
        Me.btCapNhat.Location = New System.Drawing.Point(673, 91)
        Me.btCapNhat.Name = "btCapNhat"
        Me.btCapNhat.Size = New System.Drawing.Size(75, 34)
        Me.btCapNhat.TabIndex = 29
        Me.btCapNhat.Text = "Cập nhật"
        Me.btCapNhat.UseVisualStyleBackColor = False
        '
        'btThuPhi
        '
        Me.btThuPhi.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btThuPhi.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btThuPhi.ForeColor = System.Drawing.SystemColors.Control
        Me.btThuPhi.Location = New System.Drawing.Point(673, 254)
        Me.btThuPhi.Name = "btThuPhi"
        Me.btThuPhi.Size = New System.Drawing.Size(75, 34)
        Me.btThuPhi.TabIndex = 32
        Me.btThuPhi.Text = "Thu phí"
        Me.btThuPhi.UseVisualStyleBackColor = False
        '
        'btThoat
        '
        Me.btThoat.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btThoat.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btThoat.ForeColor = System.Drawing.SystemColors.Control
        Me.btThoat.Location = New System.Drawing.Point(673, 305)
        Me.btThoat.Name = "btThoat"
        Me.btThoat.Size = New System.Drawing.Size(75, 34)
        Me.btThoat.TabIndex = 33
        Me.btThoat.Text = "Thoát"
        Me.btThoat.UseVisualStyleBackColor = False
        '
        'btLuu
        '
        Me.btLuu.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btLuu.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btLuu.ForeColor = System.Drawing.SystemColors.Control
        Me.btLuu.Location = New System.Drawing.Point(673, 182)
        Me.btLuu.Name = "btLuu"
        Me.btLuu.Size = New System.Drawing.Size(75, 34)
        Me.btLuu.TabIndex = 34
        Me.btLuu.Text = "Lưu"
        Me.btLuu.UseVisualStyleBackColor = False
        '
        'btHuy
        '
        Me.btHuy.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btHuy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btHuy.ForeColor = System.Drawing.SystemColors.Control
        Me.btHuy.Location = New System.Drawing.Point(673, 221)
        Me.btHuy.Name = "btHuy"
        Me.btHuy.Size = New System.Drawing.Size(75, 34)
        Me.btHuy.TabIndex = 35
        Me.btHuy.Text = "Hủy"
        Me.btHuy.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(158, 365)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(51, 13)
        Me.Label8.TabIndex = 36
        Me.Label8.Text = "Tìm theo:"
        '
        'cbbTimTheo
        '
        Me.cbbTimTheo.FormattingEnabled = True
        Me.cbbTimTheo.Items.AddRange(New Object() {"Mã độc giả", "Họ tên", "CMND"})
        Me.cbbTimTheo.Location = New System.Drawing.Point(215, 362)
        Me.cbbTimTheo.Name = "cbbTimTheo"
        Me.cbbTimTheo.Size = New System.Drawing.Size(121, 21)
        Me.cbbTimTheo.TabIndex = 37
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(353, 365)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(69, 13)
        Me.Label10.TabIndex = 38
        Me.Label10.Text = "Nội dung tìm:"
        '
        'txtTim
        '
        Me.txtTim.Location = New System.Drawing.Point(428, 362)
        Me.txtTim.Name = "txtTim"
        Me.txtTim.Size = New System.Drawing.Size(194, 20)
        Me.txtTim.TabIndex = 39
        '
        'btnTim
        '
        Me.btnTim.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnTim.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnTim.ForeColor = System.Drawing.SystemColors.Control
        Me.btnTim.Location = New System.Drawing.Point(673, 357)
        Me.btnTim.Name = "btnTim"
        Me.btnTim.Size = New System.Drawing.Size(75, 34)
        Me.btnTim.TabIndex = 40
        Me.btnTim.Text = "Tìm"
        Me.btnTim.UseVisualStyleBackColor = False
        '
        'QuanLiDocGia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(965, 565)
        Me.Controls.Add(Me.btnTim)
        Me.Controls.Add(Me.txtTim)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cbbTimTheo)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.btHuy)
        Me.Controls.Add(Me.btLuu)
        Me.Controls.Add(Me.btThoat)
        Me.Controls.Add(Me.btThuPhi)
        Me.Controls.Add(Me.btCapNhat)
        Me.Controls.Add(Me.btXoa)
        Me.Controls.Add(Me.btDangKi)
        Me.Controls.Add(Me.groupInfor)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvDocGia)
        Me.Name = "QuanLiDocGia"
        Me.Text = "Quản lí độc giả"
        CType(Me.dgvDocGia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.groupInfor.ResumeLayout(False)
        Me.groupInfor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvDocGia As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents groupInfor As GroupBox
    Friend WithEvents txtSDT As TextBox
    Friend WithEvents txtTenDG As TextBox
    Friend WithEvents txtCMND As TextBox
    Friend WithEvents txtSoSachQuaHan As TextBox
    Friend WithEvents txtSoSachMuonToiDa As TextBox
    Friend WithEvents txtMaDG As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents btDangKi As Button
    Friend WithEvents btXoa As Button
    Friend WithEvents btCapNhat As Button
    Friend WithEvents btThuPhi As Button
    Friend WithEvents btThoat As Button
    Friend WithEvents txtDiaChi As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cbLoaiDG As System.Windows.Forms.ComboBox
    Friend WithEvents btLuu As System.Windows.Forms.Button
    Friend WithEvents btHuy As System.Windows.Forms.Button
    Friend WithEvents maDG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tenDG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CMND As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SDT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DiaChi As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tenloai As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SLmuontoida As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cbbTimTheo As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtTim As System.Windows.Forms.TextBox
    Friend WithEvents btnTim As System.Windows.Forms.Button
End Class
