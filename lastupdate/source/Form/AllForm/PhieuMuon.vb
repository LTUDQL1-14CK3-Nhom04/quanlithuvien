﻿Imports DTO
Imports BUS
Imports System.Globalization

Public Class PhieuMuon

    Private bus As New MuonBUS()
    Private them As Boolean
    Private sua As Boolean
    Private lst As List(Of Muon)
    Private Sub loaddata()
        lst = bus.getAll
        Dim tbl = convertToDataTable()
        dgvMuon.DataSource = tbl
        them = False
        sua = False

        groupInfor.Enabled = False
        txtMaPhieuMuon.Enabled = False

        txtMaPhieuMuon.Text = ""
        txtTenDG.Text = ""
        txtTenTL.Text = ""
        cbbMaDG.Text = ""
        cbbMaTL.Text = ""

        btnThem.Enabled = True
        btnXoa.Enabled = True
        btnCapNhat.Enabled = True

        btnLuu.Enabled = False
        btnHuy.Enabled = False

        If (dgvMuon.Rows.Count > 0) Then
            btnCapNhat.Enabled = True
            btnXoa.Enabled = True
            dgvMuon_CellClick(Nothing, Nothing)
        Else
            btnCapNhat.Enabled = False
            btnXoa.Enabled = False
        End If
    End Sub
    Private Function convertToDataTable() As DataTable
        Dim tbl = New DataTable()
        tbl.Columns.Add("mamuon")
        tbl.Columns.Add("madocgia")
        tbl.Columns.Add("tendocgia")
        tbl.Columns.Add("ngaymuon")
        tbl.Columns.Add("maTL")
        tbl.Columns.Add("tenTL")
        tbl.Columns.Add("hanmuon")
        For Each item In lst

            tbl.Rows.Add(item.mamuon, item.madocgia, item.DocGia.tenDG, item.ngaymuon, item.maTL, item.TaiLieu.tenTL, item.hanmuon)
        Next
        Return tbl

    End Function
    Private Sub dgvMuon_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvMuon.CellClick
        Dim index = dgvMuon.CurrentRow.Index
        Dim muon = lst(index)
        txtMaPhieuMuon.Text = muon.mamuon.ToString()
        cbbMaTL.Text = muon.maTL
        cbbMaDG.Text = muon.madocgia
        Date.TryParseExact(muon.hanmuon, "dd/MM/yyyy", _
                               CultureInfo.CurrentCulture, _
                               DateTimeStyles.None, _
                               dtHanMuon.Value)
        Date.TryParseExact(muon.ngaymuon, "dd/MM/yyyy", _
                               CultureInfo.CurrentCulture, _
                               DateTimeStyles.None, _
                               dtNgayMuon.Value)

    End Sub

    Private Sub PhieuMuon_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadCbb()
        loaddata()
    End Sub
    Private Sub loadCbb()
        Dim dgBus = New DocGiaBUS()
        Dim tlBus = New TaiLieuBUS()
        Dim tblDG = dgBus.loadDgvDocGia()
        Dim lstTL = tlBus.getAll()

        cbbMaDG.DataSource = tblDG
        cbbMaDG.DisplayMember = "maDG"
        cbbMaDG.ValueMember = "tenDG"

        cbbMaTL.DataSource = lstTL
        cbbMaTL.DisplayMember = "maTL"
        cbbMaTL.ValueMember = "tenTL"
    End Sub
    Private Sub btnThem_Click(sender As Object, e As EventArgs) Handles btnThem.Click
        them = True
        sua = False

        groupInfor.Enabled = True
        txtMaPhieuMuon.Enabled = True

        btnThem.Enabled = False
        btnXoa.Enabled = False
        btnCapNhat.Enabled = False

        btnLuu.Enabled = True
        btnHuy.Enabled = True
        txtMaPhieuMuon.Text = ""
        txtTenDG.Text = ""
        txtTenTL.Text = ""
        cbbMaDG.Text = ""
        cbbMaTL.Text = ""
    End Sub

    Private Sub btnCapNhat_Click(sender As Object, e As EventArgs) Handles btnCapNhat.Click
        them = False
        sua = True

        groupInfor.Enabled = True
        txtMaPhieuMuon.Enabled = False

        btnThem.Enabled = False
        btnXoa.Enabled = False
        btnCapNhat.Enabled = False

        btnLuu.Enabled = True
        btnHuy.Enabled = True
    End Sub

    Private Sub btnXoa_Click(sender As Object, e As EventArgs) Handles btnXoa.Click
        Dim result = MessageBox.Show("Bạn có muốn xóa không?", "Hỏi?", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If (result = DialogResult.Yes) Then
            Dim t As Integer = dgvMuon.CurrentCell.RowIndex

            Try
                Dim tl = New Muon()
                tl = lst(t)
                bus.xoa(tl)
                MessageBox.Show("Xóa thành công!")
                loaddata()
            Catch ex As Exception
                MessageBox.Show("Không xóa được!" + ex.Message)
            End Try
        End If
    End Sub

    Private Sub btnLuu_Click(sender As Object, e As EventArgs) Handles btnLuu.Click
        Dim muon = New Muon()
        muon.mamuon = txtMaPhieuMuon.Text
        Try

            muon.maTL = cbbMaTL.Text
        Catch ex As Exception
            MessageBox.Show("Bạn phải chọn tài liệu!")
            Return

        End Try
        Try

            muon.madocgia = cbbMaDG.Text
        Catch ex As Exception
            MessageBox.Show("Bạn phải chọn độc giả!")
            Return
        End Try
        muon.hanmuon = dtHanMuon.Value.ToString("dd/MM/yyyy")
        muon.ngaymuon = dtNgayMuon.Value.ToString("dd/MM/yyyy")

        If (them) Then
            Try
                bus.them(muon)
                MessageBox.Show("Thêm thành công!")
                loaddata()

            Catch ex As Exception
                MessageBox.Show("Lỗi! Không thêm được! Lỗi: " + ex.Message)
            End Try
        End If
        If (sua) Then
            Try
                bus.sua(muon)
                MessageBox.Show("Sửa thành công!")
                loaddata()

            Catch ex As Exception
                MessageBox.Show("Lỗi! Không sửa được! Lỗi: " + ex.Message)
            End Try
        End If
    End Sub

    Private Sub btnHuy_Click(sender As Object, e As EventArgs) Handles btnHuy.Click
        loaddata()
    End Sub

    Private Sub btnTimKiem_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnThoat_Click(sender As Object, e As EventArgs) Handles btnThoat.Click
        Me.Dispose()
    End Sub

    Private Sub cbbMaDG_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbbMaDG.SelectedValueChanged
        txtTenDG.Text = cbbMaDG.SelectedValue.ToString()
    End Sub

    Private Sub cbbMaTL_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbbMaTL.SelectedValueChanged
        txtTenTL.Text = cbbMaTL.SelectedValue.ToString()
    End Sub
End Class