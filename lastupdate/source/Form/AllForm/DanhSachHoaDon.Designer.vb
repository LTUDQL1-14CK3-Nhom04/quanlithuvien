﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DanhSachHoaDon
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvHD = New System.Windows.Forms.DataGridView()
        Me.maHD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.maDG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tenDG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.loaiDG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.phithuongnien = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.phiphat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.thanhtien = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ngaythu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvHD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label1.Location = New System.Drawing.Point(250, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(255, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "DANH SÁCH HÓA ĐƠN"
        '
        'dgvHD
        '
        Me.dgvHD.AllowUserToAddRows = False
        Me.dgvHD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHD.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.maHD, Me.maDG, Me.tenDG, Me.loaiDG, Me.phithuongnien, Me.phiphat, Me.thanhtien, Me.ngaythu})
        Me.dgvHD.GridColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.dgvHD.Location = New System.Drawing.Point(21, 112)
        Me.dgvHD.Name = "dgvHD"
        Me.dgvHD.Size = New System.Drawing.Size(729, 308)
        Me.dgvHD.TabIndex = 1
        '
        'maHD
        '
        Me.maHD.DataPropertyName = "maHD"
        Me.maHD.HeaderText = "Mã hóa đơn"
        Me.maHD.Name = "maHD"
        '
        'maDG
        '
        Me.maDG.DataPropertyName = "maDG"
        Me.maDG.HeaderText = "Mã độc giả"
        Me.maDG.Name = "maDG"
        '
        'tenDG
        '
        Me.tenDG.DataPropertyName = "tenDG"
        Me.tenDG.HeaderText = "Tên độc giả"
        Me.tenDG.Name = "tenDG"
        '
        'loaiDG
        '
        Me.loaiDG.DataPropertyName = "loaiDG"
        Me.loaiDG.HeaderText = "Loại độc giả"
        Me.loaiDG.Name = "loaiDG"
        '
        'phithuongnien
        '
        Me.phithuongnien.DataPropertyName = "phithuongnien"
        Me.phithuongnien.HeaderText = "Phí thường niên"
        Me.phithuongnien.Name = "phithuongnien"
        '
        'phiphat
        '
        Me.phiphat.DataPropertyName = "phiphat"
        Me.phiphat.HeaderText = "Phí phạt"
        Me.phiphat.Name = "phiphat"
        '
        'thanhtien
        '
        Me.thanhtien.DataPropertyName = "thanhtien"
        Me.thanhtien.HeaderText = "Thành tiền"
        Me.thanhtien.Name = "thanhtien"
        '
        'ngaythu
        '
        Me.ngaythu.DataPropertyName = "ngaythu"
        Me.ngaythu.HeaderText = "Ngày thu"
        Me.ngaythu.Name = "ngaythu"
        '
        'DanhSachHoaDon
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(773, 456)
        Me.Controls.Add(Me.dgvHD)
        Me.Controls.Add(Me.Label1)
        Me.Name = "DanhSachHoaDon"
        Me.Text = "Danh sách hóa đơn"
        CType(Me.dgvHD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvHD As System.Windows.Forms.DataGridView
    Friend WithEvents maHD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents maDG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tenDG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents loaiDG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents phithuongnien As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents phiphat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents thanhtien As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ngaythu As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
