﻿Imports BUS
Imports DTO

Public Class ThuPhi

    Dim lstDG As New List(Of DocGia)
    Dim busDG As New DocGiaBUS()
    Dim busHoaDon As New ChiTietHoaDonBUS()
    Public maDG As Integer
    Private Sub ThuPhi_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lstDG = busDG.GetAll()
        cbbMaDG.DataSource = lstDG
        cbbMaDG.DisplayMember = "maDG"
        taoHD(maDG)
    End Sub

    Private Sub cbbMaDG_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbbMaDG.SelectedValueChanged
        Dim index = cbbMaDG.SelectedIndex
        Dim dg = lstDG(index)
        txtTenDG.Text = dg.tenDG
        txtLoaiDG.Text = dg.LoaiDG1.tenloai
    End Sub

    Public Sub taoHD(maDG As String)
        Dim i = 0
        For Each item In lstDG
            If (item.maDG = maDG) Then
                cbbMaDG.SelectedIndex = i

            End If
            i += 1
        Next
    End Sub


    Private Sub btnIn_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnLuu_Click(sender As Object, e As EventArgs) Handles btnLuu.Click
        Dim hd = New DTO.ChiTietHoaDon()
        Try

            hd.maHD = txtMaPhieuThu.Text
        Catch ex As Exception
            MessageBox.Show("Lỗi! Số hóa đơn không được để trống!")
            Return
        End Try
        hd.maDG = cbbMaDG.Text
        hd.NgayThu = dtNgayThu.Value.ToString("dd/MM/yyyy")
        Try
            hd.PhiPhat = txtPhiPhat.Text
        Catch ex As Exception
            MessageBox.Show("Lỗi! Phí phạt không được để trống!")
            Return
        End Try
        Try
            hd.PhiThuongNien = txtPhiThuongNien.Text
        Catch ex As Exception
            MessageBox.Show("Lỗi! Phí thường niên không được để trống!")
            Return
        End Try
        hd.ThanhTien = txtThanhTien.Text
        Try
            busHoaDon.them(hd)
            MessageBox.Show("Thêm thành công!")
            btnLuu.Enabled = False

        Catch ex As Exception
            MessageBox.Show("Lỗi! Không thêm được! Lỗi: " + ex.Message)
        End Try

    End Sub

    Private Sub txtPhiPhat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPhiThuongNien.KeyPress, txtPhiPhat.KeyPress, txtMaPhieuThu.KeyPress
        If (Char.IsDigit(e.KeyChar) = False And Char.IsControl(e.KeyChar)) Then
            e.Handled = True
        End If
        Try

            txtThanhTien.Text = Integer.Parse(txtPhiThuongNien.Text) + Integer.Parse(txtPhiPhat.Text)
        Catch ex As Exception

        End Try
    End Sub
End Class