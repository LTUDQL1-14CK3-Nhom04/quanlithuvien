﻿Imports DTO
Imports BUS
Imports System.Globalization
Public Class PhieuTra
    Private bus As New TraBUS()
    Private busMuon As New MuonBUS()
    Private them As Boolean
    Private sua As Boolean
    Private lst As List(Of Tra)
    Private lstMuon As List(Of Muon)

    Private Sub PhieuTra_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadCbb()
        loaddata()


    End Sub
    Private Sub loaddata()

        lst = bus.getAll()
        Dim tbl = convertToDataTable()
        dgvTra.DataSource = tbl
        them = False
        sua = False

        groupInfor.Enabled = False
        txtMaPhieuTra.Enabled = False

        txtMaPhieuTra.Text = ""
        txtTenDG.Text = ""
        txtTenTL.Text = ""
        cbbMaDG.Text = ""
        cbbMaTL.Text = ""

        btnThem.Enabled = True
        btnXoa.Enabled = True
        btnCapNhat.Enabled = True

        btnLuu.Enabled = False
        btnHuy.Enabled = False

        If (dgvTra.Rows.Count > 0) Then
            btnCapNhat.Enabled = True
            btnXoa.Enabled = True
            dgvTra_CellClick(Nothing, Nothing)
        Else
            btnCapNhat.Enabled = False
            btnXoa.Enabled = False
        End If
    End Sub
    Private Sub loadCbb()
        lstMuon = busMuon.getAll()
        cbbPhieuMuon.DataSource = lstMuon
        cbbPhieuMuon.DisplayMember = "mamuon"
        cbbPhieuMuon.ValueMember = "mamuon"

        Dim dgBus = New DocGiaBUS()
        Dim tlBus = New TaiLieuBUS()
        Dim tblDG = dgBus.loadDgvDocGia()
        Dim lstTL = tlBus.getAll()

        cbbMaDG.DataSource = tblDG
        cbbMaDG.DisplayMember = "maDG"
        cbbMaDG.ValueMember = "tenDG"

        cbbMaTL.DataSource = lstTL
        cbbMaTL.DisplayMember = "maTL"
        cbbMaTL.ValueMember = "tenTL"
    End Sub
    Private Function convertToDataTable() As DataTable
        Dim tbl = New DataTable()

        tbl.Columns.Add("matra")
        tbl.Columns.Add("ngaytra")
        tbl.Columns.Add("mamuon")
        tbl.Columns.Add("madocgia")
        tbl.Columns.Add("tendocgia")
        tbl.Columns.Add("ngaymuon")
        tbl.Columns.Add("maTL")
        tbl.Columns.Add("tenTL")
        tbl.Columns.Add("hanmuon")
        For Each item In lst

            tbl.Rows.Add(item.matra, item.ngaytra, item.Muon.mamuon, item.Muon.madocgia, item.Muon.DocGia.tenDG, item.Muon.ngaymuon, item.Muon.maTL, item.Muon.TaiLieu.tenTL, item.Muon.hanmuon)
        Next
        Return tbl

    End Function

    Private Sub dgvTra_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTra.CellClick
        Dim tra = lst(dgvTra.CurrentRow.Index)
        txtMaPhieuTra.Text = tra.matra
        Date.TryParseExact(tra.ngaytra, "dd/MM/yyyy", _
                               CultureInfo.CurrentCulture, _
                               DateTimeStyles.None, _
                               dtHanMuon.Value)
        cbbPhieuMuon.Text = tra.mamuon
        cbbPhieuMuon_SelectedValueChanged(Nothing, Nothing)

        cbbMaDG_SelectedValueChanged_1(Nothing, Nothing)
        cbbMaTL_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub cbbPhieuMuon_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbbPhieuMuon.SelectedValueChanged
        Dim muon = lstMuon(cbbPhieuMuon.SelectedIndex)
        cbbMaTL.Text = muon.maTL
        cbbMaDG.Text = muon.madocgia
        Date.TryParseExact(muon.hanmuon, "dd/MM/yyyy", _
                               CultureInfo.CurrentCulture, _
                               DateTimeStyles.None, _
                               dtHanMuon.Value)
        Date.TryParseExact(muon.ngaymuon, "dd/MM/yyyy", _
                               CultureInfo.CurrentCulture, _
                               DateTimeStyles.None, _
                               dtNgayMuon.Value)
    End Sub

   

    Private Sub cbbMaTL_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbMaTL.SelectedIndexChanged
        txtTenTL.Text = cbbMaTL.SelectedValue.ToString()
    End Sub

    Private Sub btnThem_Click(sender As Object, e As EventArgs) Handles btnThem.Click
        them = True
        sua = False

        groupInfor.Enabled = True
        txtMaPhieuTra.Enabled = True

        btnThem.Enabled = False
        btnXoa.Enabled = False
        btnCapNhat.Enabled = False

        btnLuu.Enabled = True
        btnHuy.Enabled = True
        cbbPhieuMuon.Text = ""
        txtMaPhieuTra.Text = ""
        txtTenDG.Text = ""
        txtTenTL.Text = ""
        cbbMaDG.Text = ""
        cbbMaTL.Text = ""
    End Sub

    Private Sub btnCapNhat_Click(sender As Object, e As EventArgs) Handles btnCapNhat.Click
        them = False
        sua = True

        groupInfor.Enabled = True
        txtMaPhieuTra.Enabled = False

        btnThem.Enabled = False
        btnXoa.Enabled = False
        btnCapNhat.Enabled = False

        btnLuu.Enabled = True
        btnHuy.Enabled = True
    End Sub

    Private Sub btnXoa_Click(sender As Object, e As EventArgs) Handles btnXoa.Click
        Dim result = MessageBox.Show("Bạn có muốn xóa không?", "Hỏi?", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If (result = DialogResult.Yes) Then
            Dim t As Integer = dgvTra.CurrentCell.RowIndex

            Try
                Dim tl = New Tra()
                tl = lst(t)
                bus.xoa(tl)
                MessageBox.Show("Xóa thành công!")
                loaddata()
            Catch ex As Exception
                MessageBox.Show("Không xóa được!" + ex.Message)
            End Try
        End If
    End Sub

    Private Sub btnLuu_Click(sender As Object, e As EventArgs) Handles btnLuu.Click
        Dim tra = New Tra()
        tra.matra = txtMaPhieuTra.Text
        tra.mamuon = cbbPhieuMuon.Text
        tra.ngaytra = dtNgayTra.Value.ToString("dd/MM/yyyy")
        If (them) Then
            Try
                bus.them(tra)
                MessageBox.Show("Thêm thành công!")
                loaddata()
            Catch ex As Exception
                MessageBox.Show("Lỗi! Không thêm được! Lỗi: " + ex.Message)
            End Try
        End If
        If (sua) Then
            Try
                bus.sua(tra)
                MessageBox.Show("Sửa thành công!")
                loaddata()
            Catch ex As Exception
                MessageBox.Show("Lỗi! Không sửa được! Lỗi: " + ex.Message)
            End Try
        End If
    End Sub

    Private Sub btnHuy_Click(sender As Object, e As EventArgs) Handles btnHuy.Click
        loaddata()
    End Sub

    Private Sub btnTimKiem_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnThoat_Click(sender As Object, e As EventArgs) Handles btnThoat.Click
        Me.Dispose()
    End Sub

    

    Private Sub cbbMaDG_SelectedValueChanged_1(sender As Object, e As EventArgs) Handles cbbMaDG.SelectedValueChanged

        txtTenDG.Text = cbbMaDG.SelectedValue.ToString()

    End Sub

    Private Sub cbbMaDG_TextChanged_1(sender As Object, e As EventArgs) Handles cbbMaDG.TextChanged
        Try
            txtTenDG.Text = cbbMaDG.SelectedValue.ToString()
        Catch ex As Exception

        End Try
    End Sub
End Class