﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PhieuTra
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnHuy = New System.Windows.Forms.Button()
        Me.groupInfor = New System.Windows.Forms.GroupBox()
        Me.cbbPhieuMuon = New System.Windows.Forms.ComboBox()
        Me.dtNgayTra = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtHanMuon = New System.Windows.Forms.DateTimePicker()
        Me.dtNgayMuon = New System.Windows.Forms.DateTimePicker()
        Me.cbbMaTL = New System.Windows.Forms.ComboBox()
        Me.cbbMaDG = New System.Windows.Forms.ComboBox()
        Me.txtMaPhieuTra = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtTenTL = New System.Windows.Forms.TextBox()
        Me.txtTenDG = New System.Windows.Forms.TextBox()
        Me.btnLuu = New System.Windows.Forms.Button()
        Me.dgvTra = New System.Windows.Forms.DataGridView()
        Me.matra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ngaytra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mamuon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.madocgia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tenDG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.maTL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TenTL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ngaymuon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hanmuon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnThoat = New System.Windows.Forms.Button()
        Me.btnThem = New System.Windows.Forms.Button()
        Me.btnCapNhat = New System.Windows.Forms.Button()
        Me.btnXoa = New System.Windows.Forms.Button()
        Me.groupInfor.SuspendLayout()
        CType(Me.dgvTra, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label9.Location = New System.Drawing.Point(284, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(160, 31)
        Me.Label9.TabIndex = 23
        Me.Label9.Text = "PHIẾU TRẢ"
        '
        'btnHuy
        '
        Me.btnHuy.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnHuy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnHuy.ForeColor = System.Drawing.SystemColors.Control
        Me.btnHuy.Location = New System.Drawing.Point(583, 201)
        Me.btnHuy.Name = "btnHuy"
        Me.btnHuy.Size = New System.Drawing.Size(75, 30)
        Me.btnHuy.TabIndex = 55
        Me.btnHuy.Text = "Hủy"
        Me.btnHuy.UseVisualStyleBackColor = False
        '
        'groupInfor
        '
        Me.groupInfor.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.groupInfor.Controls.Add(Me.cbbPhieuMuon)
        Me.groupInfor.Controls.Add(Me.dtNgayTra)
        Me.groupInfor.Controls.Add(Me.Label7)
        Me.groupInfor.Controls.Add(Me.dtHanMuon)
        Me.groupInfor.Controls.Add(Me.dtNgayMuon)
        Me.groupInfor.Controls.Add(Me.cbbMaTL)
        Me.groupInfor.Controls.Add(Me.cbbMaDG)
        Me.groupInfor.Controls.Add(Me.txtMaPhieuTra)
        Me.groupInfor.Controls.Add(Me.Label1)
        Me.groupInfor.Controls.Add(Me.Label2)
        Me.groupInfor.Controls.Add(Me.Label3)
        Me.groupInfor.Controls.Add(Me.Label4)
        Me.groupInfor.Controls.Add(Me.Label5)
        Me.groupInfor.Controls.Add(Me.Label6)
        Me.groupInfor.Controls.Add(Me.txtTenTL)
        Me.groupInfor.Controls.Add(Me.txtTenDG)
        Me.groupInfor.Location = New System.Drawing.Point(74, 43)
        Me.groupInfor.Name = "groupInfor"
        Me.groupInfor.Size = New System.Drawing.Size(450, 285)
        Me.groupInfor.TabIndex = 57
        Me.groupInfor.TabStop = False
        Me.groupInfor.Text = "Thông tin mượn"
        '
        'cbbPhieuMuon
        '
        Me.cbbPhieuMuon.FormattingEnabled = True
        Me.cbbPhieuMuon.Location = New System.Drawing.Point(117, 84)
        Me.cbbPhieuMuon.Name = "cbbPhieuMuon"
        Me.cbbPhieuMuon.Size = New System.Drawing.Size(152, 21)
        Me.cbbPhieuMuon.TabIndex = 23
        '
        'dtNgayTra
        '
        Me.dtNgayTra.CustomFormat = "dd/MM/yyyy"
        Me.dtNgayTra.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtNgayTra.Location = New System.Drawing.Point(117, 51)
        Me.dtNgayTra.Name = "dtNgayTra"
        Me.dtNgayTra.Size = New System.Drawing.Size(152, 20)
        Me.dtNgayTra.TabIndex = 22
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 51)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 13)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "Ngày trả"
        '
        'dtHanMuon
        '
        Me.dtHanMuon.CustomFormat = "dd/MM/yyyy"
        Me.dtHanMuon.Enabled = False
        Me.dtHanMuon.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtHanMuon.Location = New System.Drawing.Point(117, 245)
        Me.dtHanMuon.Name = "dtHanMuon"
        Me.dtHanMuon.Size = New System.Drawing.Size(152, 20)
        Me.dtHanMuon.TabIndex = 20
        '
        'dtNgayMuon
        '
        Me.dtNgayMuon.CustomFormat = "dd/MM/yyyy"
        Me.dtNgayMuon.Enabled = False
        Me.dtNgayMuon.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtNgayMuon.Location = New System.Drawing.Point(117, 202)
        Me.dtNgayMuon.Name = "dtNgayMuon"
        Me.dtNgayMuon.Size = New System.Drawing.Size(152, 20)
        Me.dtNgayMuon.TabIndex = 19
        '
        'cbbMaTL
        '
        Me.cbbMaTL.Enabled = False
        Me.cbbMaTL.FormattingEnabled = True
        Me.cbbMaTL.Location = New System.Drawing.Point(117, 148)
        Me.cbbMaTL.Name = "cbbMaTL"
        Me.cbbMaTL.Size = New System.Drawing.Size(152, 21)
        Me.cbbMaTL.TabIndex = 18
        '
        'cbbMaDG
        '
        Me.cbbMaDG.Enabled = False
        Me.cbbMaDG.FormattingEnabled = True
        Me.cbbMaDG.Location = New System.Drawing.Point(117, 113)
        Me.cbbMaDG.Name = "cbbMaDG"
        Me.cbbMaDG.Size = New System.Drawing.Size(152, 21)
        Me.cbbMaDG.TabIndex = 18
        '
        'txtMaPhieuTra
        '
        Me.txtMaPhieuTra.Location = New System.Drawing.Point(117, 19)
        Me.txtMaPhieuTra.Name = "txtMaPhieuTra"
        Me.txtMaPhieuTra.Size = New System.Drawing.Size(152, 20)
        Me.txtMaPhieuTra.TabIndex = 11
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Mã phiếu trả"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Mã phiếu mượn"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 202)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Ngày mượn"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 155)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Mã tài liệu"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 120)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Mã độc giả"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 251)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Hạn mượn"
        '
        'txtTenTL
        '
        Me.txtTenTL.Enabled = False
        Me.txtTenTL.Location = New System.Drawing.Point(275, 148)
        Me.txtTenTL.Name = "txtTenTL"
        Me.txtTenTL.Size = New System.Drawing.Size(152, 20)
        Me.txtTenTL.TabIndex = 17
        '
        'txtTenDG
        '
        Me.txtTenDG.Enabled = False
        Me.txtTenDG.Location = New System.Drawing.Point(275, 113)
        Me.txtTenDG.Name = "txtTenDG"
        Me.txtTenDG.Size = New System.Drawing.Size(152, 20)
        Me.txtTenDG.TabIndex = 17
        '
        'btnLuu
        '
        Me.btnLuu.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnLuu.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnLuu.ForeColor = System.Drawing.SystemColors.Control
        Me.btnLuu.Location = New System.Drawing.Point(583, 172)
        Me.btnLuu.Name = "btnLuu"
        Me.btnLuu.Size = New System.Drawing.Size(75, 30)
        Me.btnLuu.TabIndex = 54
        Me.btnLuu.Text = "Lưu"
        Me.btnLuu.UseVisualStyleBackColor = False
        '
        'dgvTra
        '
        Me.dgvTra.AllowUserToAddRows = False
        Me.dgvTra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTra.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.matra, Me.ngaytra, Me.mamuon, Me.madocgia, Me.tenDG, Me.maTL, Me.TenTL, Me.ngaymuon, Me.hanmuon})
        Me.dgvTra.Location = New System.Drawing.Point(17, 334)
        Me.dgvTra.Name = "dgvTra"
        Me.dgvTra.Size = New System.Drawing.Size(741, 150)
        Me.dgvTra.TabIndex = 56
        '
        'matra
        '
        Me.matra.DataPropertyName = "matra"
        Me.matra.HeaderText = "Mã phiếu trả"
        Me.matra.Name = "matra"
        '
        'ngaytra
        '
        Me.ngaytra.DataPropertyName = "ngaytra"
        Me.ngaytra.HeaderText = "Ngày trả"
        Me.ngaytra.Name = "ngaytra"
        '
        'mamuon
        '
        Me.mamuon.DataPropertyName = "mamuon"
        Me.mamuon.HeaderText = "Mã phiếu mượn"
        Me.mamuon.Name = "mamuon"
        '
        'madocgia
        '
        Me.madocgia.DataPropertyName = "madocgia"
        Me.madocgia.HeaderText = "Mã độc giả"
        Me.madocgia.Name = "madocgia"
        '
        'tenDG
        '
        Me.tenDG.DataPropertyName = "tendocgia"
        Me.tenDG.HeaderText = "Tên độc giả"
        Me.tenDG.Name = "tenDG"
        '
        'maTL
        '
        Me.maTL.DataPropertyName = "maTL"
        Me.maTL.HeaderText = "Mã tài liệu"
        Me.maTL.Name = "maTL"
        '
        'TenTL
        '
        Me.TenTL.DataPropertyName = "tenTL"
        Me.TenTL.HeaderText = "Tên tài liệu"
        Me.TenTL.Name = "TenTL"
        '
        'ngaymuon
        '
        Me.ngaymuon.DataPropertyName = "ngaymuon"
        Me.ngaymuon.HeaderText = "Ngày mượn"
        Me.ngaymuon.Name = "ngaymuon"
        '
        'hanmuon
        '
        Me.hanmuon.DataPropertyName = "hanmuon"
        Me.hanmuon.HeaderText = "Hạn mượn"
        Me.hanmuon.Name = "hanmuon"
        '
        'btnThoat
        '
        Me.btnThoat.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnThoat.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnThoat.ForeColor = System.Drawing.SystemColors.Control
        Me.btnThoat.Location = New System.Drawing.Point(583, 235)
        Me.btnThoat.Name = "btnThoat"
        Me.btnThoat.Size = New System.Drawing.Size(75, 30)
        Me.btnThoat.TabIndex = 53
        Me.btnThoat.Text = "Thoát"
        Me.btnThoat.UseVisualStyleBackColor = False
        '
        'btnThem
        '
        Me.btnThem.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnThem.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnThem.ForeColor = System.Drawing.SystemColors.Control
        Me.btnThem.Location = New System.Drawing.Point(583, 71)
        Me.btnThem.Name = "btnThem"
        Me.btnThem.Size = New System.Drawing.Size(75, 30)
        Me.btnThem.TabIndex = 49
        Me.btnThem.Text = "Thêm"
        Me.btnThem.UseVisualStyleBackColor = False
        '
        'btnCapNhat
        '
        Me.btnCapNhat.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnCapNhat.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnCapNhat.ForeColor = System.Drawing.SystemColors.Control
        Me.btnCapNhat.Location = New System.Drawing.Point(583, 105)
        Me.btnCapNhat.Name = "btnCapNhat"
        Me.btnCapNhat.Size = New System.Drawing.Size(75, 30)
        Me.btnCapNhat.TabIndex = 51
        Me.btnCapNhat.Text = "Cập nhật"
        Me.btnCapNhat.UseVisualStyleBackColor = False
        '
        'btnXoa
        '
        Me.btnXoa.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnXoa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnXoa.ForeColor = System.Drawing.SystemColors.Control
        Me.btnXoa.Location = New System.Drawing.Point(583, 140)
        Me.btnXoa.Name = "btnXoa"
        Me.btnXoa.Size = New System.Drawing.Size(75, 30)
        Me.btnXoa.TabIndex = 50
        Me.btnXoa.Text = "Xóa"
        Me.btnXoa.UseVisualStyleBackColor = False
        '
        'PhieuTra
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(772, 496)
        Me.Controls.Add(Me.btnHuy)
        Me.Controls.Add(Me.groupInfor)
        Me.Controls.Add(Me.btnLuu)
        Me.Controls.Add(Me.dgvTra)
        Me.Controls.Add(Me.btnThoat)
        Me.Controls.Add(Me.btnThem)
        Me.Controls.Add(Me.btnCapNhat)
        Me.Controls.Add(Me.btnXoa)
        Me.Controls.Add(Me.Label9)
        Me.Name = "PhieuTra"
        Me.Text = "Quản lí trả"
        Me.groupInfor.ResumeLayout(False)
        Me.groupInfor.PerformLayout()
        CType(Me.dgvTra, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btnHuy As System.Windows.Forms.Button
    Friend WithEvents groupInfor As System.Windows.Forms.GroupBox
    Friend WithEvents dtHanMuon As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtNgayMuon As System.Windows.Forms.DateTimePicker
    Friend WithEvents cbbMaTL As System.Windows.Forms.ComboBox
    Friend WithEvents cbbMaDG As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtTenTL As System.Windows.Forms.TextBox
    Friend WithEvents txtTenDG As System.Windows.Forms.TextBox
    Friend WithEvents btnLuu As System.Windows.Forms.Button
    Friend WithEvents dgvTra As System.Windows.Forms.DataGridView
    Friend WithEvents btnThoat As System.Windows.Forms.Button
    Friend WithEvents btnThem As System.Windows.Forms.Button
    Friend WithEvents btnCapNhat As System.Windows.Forms.Button
    Friend WithEvents btnXoa As System.Windows.Forms.Button
    Friend WithEvents cbbPhieuMuon As System.Windows.Forms.ComboBox
    Friend WithEvents dtNgayTra As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtMaPhieuTra As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents matra As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ngaytra As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents mamuon As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents madocgia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tenDG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents maTL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TenTL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ngaymuon As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hanmuon As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
