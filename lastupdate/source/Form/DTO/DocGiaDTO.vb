﻿Public Class DocGiaDTO
#Region "attributes"
    Private _maDG As Integer
    Private _tenDG As String
    Private _loaiDG As Integer
    Private _CMND As String
    Private _SDT As String
    Private _gioitinh As String
    Private _diachi As String

#End Region
#Region "Properties"

    Public Property maDG() As Integer

        Get
            Return _maDG
        End Get
        Set(ByVal value As Integer)
            _maDG = value
        End Set
    End Property

    Public Property tenDG() As String
        Get
            Return _tenDG
        End Get
        Set(ByVal value As String)
            _tenDG = value
        End Set
    End Property

    Public Property loaiDG() As Integer
        Get
            Return _loaiDG
        End Get
        Set(ByVal value As Integer)
            _loaiDG = value
        End Set
    End Property

    Public Property CMND() As String
        Get
            Return _CMND
        End Get
        Set(ByVal value As String)
            _CMND = value
        End Set
    End Property

    Public Property SDT() As String
        Get
            Return _SDT
        End Get
        Set(ByVal value As String)
            _SDT = value
        End Set
    End Property

    Public Property gioitinh() As String
        Get
            Return _gioitinh
        End Get
        Set(ByVal value As String)
            _gioitinh = value
        End Set
    End Property

    Public Property diachi() As String
        Get
            Return _diachi
        End Get
        Set(ByVal value As String)
            _diachi = value
        End Set
    End Property
#End Region

End Class
