﻿
Public Class DangNhapDTO
#Region "attributes"
    Private _username As String
    Private _password As String


#End Region
#Region "Properties"

    Public Property username() As String
        Get
            Return _username
        End Get
        Set(ByVal value As String)
            _username = value
        End Set
    End Property

    Public Property password() As String
        Get
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
        End Set
    End Property
#End Region

End Class
