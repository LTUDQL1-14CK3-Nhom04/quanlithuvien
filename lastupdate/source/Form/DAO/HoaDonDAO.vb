﻿Imports DTO
Public Class HoaDonDAO
    Private dbs As New DALDataContext()
    Public Function getAll() As List(Of ChiTietHoaDon)
        Return dbs.ChiTietHoaDons.ToList()
    End Function
    Public Function them(item As ChiTietHoaDon) As Boolean
        dbs = New DALDataContext()
        dbs.ChiTietHoaDons.InsertOnSubmit(item)
        dbs.SubmitChanges()
        Return True

    End Function
    Public Function sua(item As ChiTietHoaDon) As Boolean
        dbs = New DALDataContext()
        Dim old = (From c In dbs.ChiTietHoaDons
                Where c.maHD = item.maHD
                Select c).First
        old.maDG = item.maDG
        old.NgayThu = item.NgayThu
        old.PhiPhat = item.PhiPhat
        old.PhiThuongNien = item.PhiThuongNien
        old.ThanhTien = item.ThanhTien

        dbs.SubmitChanges()
        Return True
    End Function
    Public Function xoa(item As ChiTietHoaDon) As Boolean
        dbs = New DALDataContext()
        Dim old = (From c In dbs.ChiTietHoaDons
                Where c.maHD = item.maHD
                Select c).First
        dbs.ChiTietHoaDons.DeleteOnSubmit(old)
        dbs.SubmitChanges()
        Return True

    End Function
End Class
